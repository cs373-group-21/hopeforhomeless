import React from "react";
import "./App.css";
import "./style.css";
// Using code from the Find Events Near Me project (https://gitlab.com/gpravelan/cs373-idb)
// Also using https://www.techomoro.com/how-to-create-a-multi-page-website-with-react-in-5-minutes/ as a reference

import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import NavB from "./components/NavB";
// import Charitable_organizations from "./components/Charitable_organizations";
import Charitable_organizations_instance from "./components/Charitable_organizations_instance";
import About from "./components/About";
import Splash from "./components/Splash";
import Location_instance from "./components/Location_instance";
import School_instance from "./components/School_instance";
import Org_Model_Page from "./components/OrgModelPage/Org_Model_Page";
import School_Model_Page from "./components/SchoolModelPage/School_Model_Page";
import City_Model_Page from "./components/CitiesModelPage/City_Model_Page";
import Org_Vis from "./components/Visualizations/OrgVis";
import School_Vis from "./components/Visualizations/SchoolVis";
import City_Vis from "./components/Visualizations/CityVis";
import Search_page from "./components/Search_page";
import Visualization from "./components/Visualizations/Visualization";
import PVisualization from "./components/Visualizations/PVisualization";
function App() {
  return (
    <div className="App">
      <Router>
        <NavB />
        <Switch>
          <Route exact path="/" component={Splash} />
          <Route exact path="/about" component={About} />
          <Route
            exact
            path="/locations/n=:name?/s=:state?/po=:population?/pv=:poverty?/i=:income?"
            component={City_Model_Page}
          />
          <Route
            exact
            path="/organizations/n=:name?/ct=:city?/ca=:category?/z=:zip?/i=:income?"
            component={Org_Model_Page}
          />
          <Route
            exact
            path="/schools/n=:name?/s=:state?/d=:district?/l=:level?/r=:rank?"
            component={School_Model_Page}
          />
          <Route path="/schools/:id" component={School_instance} />
          <Route
            path="/organizations/:id"
            component={Charitable_organizations_instance}
          />
          <Route path="/locations/:id" component={Location_instance} />
          <Route path="/search/q=:q/filter=:filter" component={Search_page} />
          <Route path="/visualizations/hfh/:v" component={Visualization} />
          <Route
            path="/visualizations/provider/:vis"
            component={PVisualization}
          />
          <Route path="/visualizations/organizations" component={Org_Vis} />
          <Route path="/visualizations/schools" component={School_Vis} />
          <Route path="/visualizations/cities" component={City_Vis} />
        </Switch>
      </Router>
    </div>
  );
}

export default App;
