fetch(
  `https://gitlab.com/api/v4/projects/24704262/?statistics=true&private_token=${env.ACCESS_TOKEN}`
)
  .then((response) => response.json())
  .then((data) => {
    document.getElementById("commit").innerHTML += data.statistics.commit_count;
  });

fetch(
  `https://gitlab.com/api/v4/projects/24704262/issues?private_token=${env.ACCESS_TOKEN}`
)
  .then((response) => response.json())
  .then((data) => {
    document.getElementById("issues").innerHTML += data.length;
    const usernames = [
      "Shikhar-G",
      "nngalloso",
      "sruthi_rudravajjala",
      "maradiai",
      "trumanbyrd",
    ];
    const names = ["shikhar", "noah", "sruthi", "shayan", "truman"];
    let authors = new Array(5).fill(0);
    let closers = new Array(5).fill(0);
    for (var issue in data) {
      let author = usernames.indexOf(data[issue].author.username);
      if (author != -1) authors[author]++;
      if (data[issue].state == "closed") {
        let closer = usernames.indexOf(data[issue].closed_by.username);
        if (closer != -1) closers[closer]++;
      }
    }
    for (var i = 0; i < 5; i++) {
      document.getElementById(`${names[i]}IssuesA`).innerHTML += authors[i];
      document.getElementById(`${names[i]}IssuesC`).innerHTML += closers[i];
    }
  });

fetch(
  `https://gitlab.com/api/v4/projects/24704262/repository/commits?per_page=1000&all=TRUE&private_token=${env.ACCESS_TOKEN}`
)
  .then((response) => response.json())
  .then((data) => {
    const names = [
      "Shikhar Gupta",
      "Noah Galloso",
      "Sruthi Rudravajjala",
      "Shayan Maradia",
      "Truman Byrd",
    ];
    const ids = ["shikhar", "noah", "sruthi", "shayan", "truman"];
    let authors = new Array(5).fill(0);
    for (var commit in data) {
      let author = names.indexOf(data[commit].author_name);
      if (author != -1) authors[author]++;
    }
    for (var i = 0; i < 5; i++) {
      document.getElementById(`${ids[i]}Commits`).innerHTML += authors[i];
    }
  });
