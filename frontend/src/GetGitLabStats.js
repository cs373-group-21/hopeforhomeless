import React from "react";
import "../style.css";
import "../App.css";

import Noah from "../media/headshots/Noah_Image.jpeg";
import Shayan from "../media/headshots/Shayan_Image.jpg";
import Shikhar from "../media/headshots/Shikhar_Image.jpg";
import Sruthi from "../media/headshots/Sruthi_Image.jpg";
import Truman from "../media/headshots/Truman_Image.png";

import Teams from "../media/tools/teams.png";
import Postman from "../media/tools/postman.png";
import Gitlab from "../media/tools/gitlab.jpeg";
import NameCheap from "../media/tools/namecheap.png";
import AWS from "../media/tools/aws.png";

// import gitjs from "../gitlab"
// import environ from "../.env"

function About() {
  const [error, setError] = useState(null);
  const [isLoaded, setIsLoaded] = useState(false);
  const [items, setItems] = useState([]);

  useEffect(() => {
    fetch()
      .then((res) => res.json())
      .then(
        (result) => {
          setIsLoaded(true);
          setItems(result);
        },
        // Note: it's important to handle errors here
        // instead of a catch() block so that we don't swallow
        // exceptions from actual bugs in components.
        (error) => {
          setIsLoaded(true);
          setError(error);
        }
      );
  }, []);

  return (
    <div className="About">
      <body>
        <header>
          <div id="navigation"></div>
        </header>

        <main>
          <script src="/src/.env.js"></script>
          <script src="/src/gitlab.js"></script>
          {/* <script src={environ}></script>
          <script src={gitjs}></script> */}

          <div className="content">
            <h1>About</h1>
            <div className="descriptionContainer">
              <h3>Our Mission</h3>
              <p>
                Hope for Homeless's mission is twofold: we seek to educate the
                general public about the state of homelessness and poverty in
                major cities, and we also aim to provide information about
                available organizations that can assist those in need. There is
                a serious lack of information about the impoverished population
                as well as available resources in major cities, and we hope to
                bridge this gap.
              </p>
            </div>
            <div className="container">
              <h3>Meet the Team</h3>
            </div>
            <div className="cardFlexContainer" styles="margin-top: 0px;">
              <div className="row">
                <div className="col-auto">
                  <div className="card">
                    <div className="ant-card ant-card-bordered ant-card-hoverable Splash_card__1rIR5">
                      <div className="row">
                        <h2 className="ant-typography">Shikhar Gupta</h2>
                      </div>
                      <div className="row">
                        <div className="ant-card-cover">
                          <img
                            className="cardImage"
                            alt="Shikhar"
                            src={Shikhar}
                          />
                        </div>
                      </div>
                      <div className="row">
                        <div className="descriptionContainer">
                          <p>
                            Shikhar has a passion for alleviating poverty, is a
                            sophomore, and loves programming!
                          </p>
                          <p>
                            Shikhar's major responsibilities included working on
                            the back-end.
                          </p>
                          <p id="shikharCommits">Commits: </p>
                          <p id="shikharIssuesA">Issues Authored: </p>
                          <p id="shikharIssuesC">Issues Closed: </p>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div className="col-auto">
                <div className="card">
                  <div className="ant-card ant-card-bordered ant-card-hoverable Splash_card__1rIR5">
                    <div className="row">
                      <h2 className="ant-typography">Sruthi Rudravajjala</h2>
                    </div>
                    <div className="row">
                      <div className="ant-card-cover">
                        <img className="cardImage" alt="Sruthi" src={Sruthi} />
                      </div>
                    </div>
                    <div className="row">
                      <div className="descriptionContainer">
                        <p>
                          Sruthi has a passion for community service and helping
                          people in need, is a Junior, and loves technology!
                        </p>
                        <p>
                          Sruthi's major responsibilities included working on
                          the front-end.
                        </p>
                        <p id="sruthiCommits">Commits: </p>
                        <p id="sruthiIssuesA">Issues Authored: </p>
                        <p id="sruthiIssuesC">Issues Closed: </p>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div className="col-auto">
                <div className="card">
                  <div className="ant-card ant-card-bordered ant-card-hoverable Splash_card__1rIR5">
                    <div className="row">
                      <h2 className="ant-typography">Truman Byrd</h2>
                    </div>
                    <div className="row">
                      <div className="ant-card-cover">
                        <img className="cardImage" alt="Truman" src={Truman} />
                      </div>
                    </div>
                    <div className="row">
                      <div className="descriptionContainer">
                        <p>
                          Truman is passionate about helping homeless people, is
                          a Junior, and likes to skateboard!
                        </p>
                        <p>
                          Truman's major responsibilities included working on
                          the back-end.
                        </p>
                        <p id="trumanCommits">Commits: </p>
                        <p id="trumanIssuesA">Issues Authored: </p>
                        <p id="trumanIssuesC">Issues Closed: </p>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div className="col-auto">
                <div className="card">
                  <div className="ant-card ant-card-bordered ant-card-hoverable Splash_card__1rIR5">
                    <div className="row">
                      <h2 className="ant-typography">Noah Galloso</h2>
                    </div>
                    <div className="row">
                      <div className="ant-card-cover">
                        <img className="cardImage" alt="Noah" src={Noah} />
                      </div>
                    </div>
                    <div className="row">
                      <div className="descriptionContainer">
                        <p>
                          Noah is passionate about helping people who are less
                          fortunate, is a Senior, and plays the guitar!
                        </p>
                        <p>
                          Noah's major responsibilities included working on the
                          back-end.
                        </p>
                        <p id="noahCommits">Commits: </p>
                        <p id="noahIssuesA">Issues Authored: </p>
                        <p id="noahIssuesC">Issues Closed: </p>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div className="col-auto">
                <div className="card">
                  <div className="ant-card ant-card-bordered ant-card-hoverable Splash_card__1rIR5">
                    <div className="row">
                      <h2 className="ant-typography">Shayan Maradia</h2>
                    </div>
                    <div className="row">
                      <div className="ant-card-cover">
                        <img className="cardImage" alt="Shayan" src={Shayan} />
                      </div>
                    </div>
                    <div className="row">
                      <div className="descriptionContainer">
                        <p>
                          Shayan is passionate about improving the quality of
                          life of impoverished communities, is a junior, and
                          likes to play basketball!
                        </p>
                        <p>
                          Shayan's major responsibilities included working on
                          the front-end.
                        </p>
                        <p id="shayanCommits">Commits: </p>
                        <p id="shayanIssuesA">Issues Authored: </p>
                        <p id="shayanIssuesC">Issues Closed: </p>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="container" styles="margin-top: 2em;">
              <br></br>
              <h3>Data</h3>
              <table className="table table-striped">
                <thead>
                  <th scope="col">Source</th>
                  <th scope="col">Description</th>
                </thead>
                <tbody>
                  <tr>
                    <td>
                      <a href="https://projects.propublica.org/nonprofits/api">
                        ProPublica Nonprofits API
                      </a>
                    </td>
                    <td>
                      Used to gather information about charitable organizations
                      in various cities.
                    </td>
                  </tr>
                  <tr>
                    <td>
                      <a href="https://api.census.gov/data/timeseries/poverty/saipe.html">
                        Census API
                      </a>
                    </td>
                    <td>
                      Used to compile income and poverty statistics for various
                      cities around the country.
                    </td>
                  </tr>
                  <tr>
                    <td>
                      <a href="https://developers.teleport.org/api/getting_started/#life_quality_ua">
                        Quality of Life API
                      </a>
                    </td>
                    <td>
                      Used to display quality of life statistics for various
                      areas.
                    </td>
                  </tr>
                  <tr>
                    <td>
                      <a href="https://api.data.gov/">Data.gov API</a>
                    </td>
                    <td>
                      Used to gather supplemental information regarding
                      Education statistics
                    </td>
                  </tr>
                </tbody>
              </table>
            </div>
            <div className="container">
              <h3>Tools</h3>
            </div>
            <div className="cardFlexContainer">
              <div className="row">
                <div className="col-auto">
                  <a className="card card-small" href="https://aws.amazon.com/">
                    <div className="ant-card-cover">
                      <img className="cardImage" src={AWS} alt="aws" />
                    </div>
                    <div className="card-body">
                      <h5 className="card-title">AWS</h5>
                      <p className="card-text">
                        Deploy, manage the database, and route traffic.
                      </p>
                    </div>
                  </a>
                </div>
                <div className="col-auto">
                  <a
                    className="card card-small"
                    href="https://www.namecheap.com/"
                  >
                    <div className="ant-card-cover">
                      <img
                        className="cardImage"
                        src={NameCheap}
                        alt="namecheap"
                      />
                    </div>
                    <div className="card-body">
                      <h5 className="card-title">NameCheap</h5>
                      <p className="card-text">Register the domain.</p>
                    </div>
                  </a>
                </div>
                <div className="col-auto">
                  <a className="card card-small" href="https://www.gitlab.com/">
                    <div className="ant-card-cover">
                      <img className="cardImage" src={Gitlab} alt="gitlab" />
                    </div>
                    <div className="card-body">
                      <h5 className="card-title">GitLab</h5>
                      <p className="card-text">
                        Hosts the source code for the website.
                      </p>
                    </div>
                  </a>
                </div>
                <div className="col-auto">
                  <a
                    className="card card-small"
                    href="https://www.postman.com/"
                  >
                    <div className="ant-card-cover">
                      <img className="cardImage" src={Postman} alt="postman" />
                    </div>
                    <div className="card-body">
                      <h5 className="card-title">Postman</h5>
                      <p className="card-text">
                        Define and implement the API for the backend.
                      </p>
                    </div>
                  </a>
                </div>
                <div className="col-auto">
                  <a
                    className="card card-small"
                    href="https://www.microsoft.com/en-us/microsoft-teams/group-chat-software"
                  >
                    <div className="ant-card-cover">
                      <img className="cardImage" src={Teams} alt="teams" />
                    </div>
                    <div className="card-body">
                      <h5 className="card-title">Microsoft Teams</h5>
                      <p className="card-text">Used for team communication.</p>
                    </div>
                  </a>
                </div>
              </div>
            </div>

            <div className="descriptionContainer">
              <h3>Statistics</h3>
              <h5 id="commit">Total Commits: </h5>
              <h5 id="issues">Total Issues: </h5>
            </div>
            {/*
            <div class="descriptionContainer">
                <a href=https://documenter.getpostman.com/view/14728622/Tz5jfM1Q>Postman API Documentation</a>
                <br>
                <a href=https://gitlab.com/cs373-group-21/hopeforhomeless>GitLab Repository</a>
            </div>
            */}
          </div>
        </main>

        <footer></footer>

        <script
          src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/js/bootstrap.bundle.min.js"
          integrity="sha384-b5kHyXgcpbZJO/tY9Ul7kGkf1S0CWuKcCD38l8YkeH8z8QjE0GmW1gYU5S9FOnJ0"
          crossorigin="anonymous"
        ></script>
      </body>
    </div>
  );
}

export default About;
