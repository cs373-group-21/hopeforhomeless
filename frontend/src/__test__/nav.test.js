//https://www.youtube.com/watch?v=3e1GHCA3GP0
//https://www.npmjs.com/package/@testing-library/jest-dom
import React from 'react';
import ReactDOM from 'react-dom';
import NavB from '../components/NavB';

import {render, cleanup} from '@testing-library/react';
import '@testing-library/jest-dom';

//https://reactjs.org/docs/test-renderer.html
import TestRenderer from "react-test-renderer";


afterEach(cleanup);
it ("renders without crashing", () => {
    const div = document.createElement('div');
    ReactDOM.render(<NavB></NavB>, div);
    ReactDOM.unmountComponentAtNode(div);
});

it ("matches snapshot", () =>{
    const tree = TestRenderer.create(<NavB></NavB>).toJSON();
    expect(tree).toMatchSnapshot();
});
