//https://www.youtube.com/watch?v=3e1GHCA3GP0
//https://www.npmjs.com/package/@testing-library/jest-dom
import React from 'react';
import ReactDOM from 'react-dom';
import City_Model_Page from '../components/CitiesModelPage/City_Model_Page';
import '@testing-library/jest-dom';
import { act } from "react-dom/test-utils";
import pretty from "pretty";

let container = null;
beforeEach(() => {
  container = document.createElement("div");
  document.body.appendChild(container);
});

afterEach(() => {
  // cleanup on exiting
  ReactDOM.unmountComponentAtNode(container);
  container.remove();
  container = null;
});


it ("renders and matches snapshot", async ()=> {
    await act(async () => {
        ReactDOM.render(<City_Model_Page match={{params: {name: 'a', state: 'a', population: '1', income: '1', poverty: '1' }}}  ></City_Model_Page>, container);
    });
    expect(
        pretty(container.innerHTML))
        .toMatchSnapshot();
});
