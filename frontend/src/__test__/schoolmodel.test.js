//https://www.youtube.com/watch?v=3e1GHCA3GP0
//https://www.npmjs.com/package/@testing-library/jest-dom
import React from 'react';
import ReactDOM from 'react-dom';
import School_Model_Page from '../components/SchoolModelPage/School_Model_Page';

import {cleanup} from '@testing-library/react';
import '@testing-library/jest-dom';

import { act } from 'react-dom/test-utils';
import pretty from "pretty";

let container = null;
beforeEach(() => {
  container = document.createElement("div");
  document.body.appendChild(container);
});

afterEach(() => {
  // cleanup on exiting
  ReactDOM.unmountComponentAtNode(container);
  container.remove();
  container = null;
});


afterEach(cleanup);
it ("renders and matches snapshot", async ()=> {
    await act(async () => {
        ReactDOM.render(<School_Model_Page match={{params: {name: 'a', state: 'a', district: 'a', level: 'm', rank: '1' }}}></School_Model_Page>, container);
    });
    expect(
        pretty(container.innerHTML))
        .toMatchSnapshot();
});
