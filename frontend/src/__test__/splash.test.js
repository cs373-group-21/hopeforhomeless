//https://www.youtube.com/watch?v=3e1GHCA3GP0
//https://www.npmjs.com/package/@testing-library/jest-dom
import React from 'react';
import ReactDOM from 'react-dom';
import Splash from '../components/Splash';

import {render, cleanup} from '@testing-library/react';
import '@testing-library/jest-dom';

//https://reactjs.org/docs/test-renderer.html
import TestRenderer from "react-test-renderer";


afterEach(cleanup);
it ("renders without crashing", () => {
    const div = document.createElement('div');
    ReactDOM.render(<Splash></Splash>, div);
    ReactDOM.unmountComponentAtNode(div);
});

it ("matches snapshot", () =>{
    const tree = TestRenderer.create(<Splash></Splash>).toJSON();
    expect(tree).toMatchSnapshot();
});

