import App from '../App';
import ReactDOM from 'react-dom';

//https://reactjs.org/docs/test-renderer.html
import TestRenderer from "react-test-renderer";

it ('renders app', () => {
  const div = document.createElement('App');
  ReactDOM.render(<App />, div);
  ReactDOM.unmountComponentAtNode(div);
});

it ("matches snapshot", () =>{
  const tree = TestRenderer.create(<App />).toJSON();
  expect(tree).toMatchSnapshot();
});

