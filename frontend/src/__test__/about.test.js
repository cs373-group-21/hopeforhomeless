//https://www.youtube.com/watch?v=3e1GHCA3GP0
//https://www.npmjs.com/package/@testing-library/jest-dom
import React from 'react';
import ReactDOM from 'react-dom';
import About from '../components/About';

import {render, cleanup} from '@testing-library/react';
import '@testing-library/jest-dom';

//https://reactjs.org/docs/test-renderer.html
import TestRenderer from "react-test-renderer";


afterEach(cleanup);
it ("renders without crashing", () => {
    const div = document.createElement('div');
    ReactDOM.render(<About></About>, div);
    ReactDOM.unmountComponentAtNode(div);
});

it ("renders about correctly", () => {
    const {getByTestId} = render (<About/>);
    expect(getByTestId('About')).toHaveTextContent("About");
});

it ("matches snapshot", () =>{
    const tree = TestRenderer.create(<About></About>).toJSON();
    expect(tree).toMatchSnapshot();
});
