// Using Burnin Up project as reference
import React from "react";
import "../style.css";
import "../App.css";
import Card from "@material-ui/core/Card";
import CardActions from "@material-ui/core/CardActions";
import CardContent from "@material-ui/core/CardContent";
import Typography from "@material-ui/core/Typography";
import Button from "@material-ui/core/Button";
import Grid from "@material-ui/core/Grid";
import Container from "@material-ui/core/Container";
import { Dropdown } from "react-bootstrap";

//https://www.algolia.com/doc/guides/building-search-ui/installation/react/
import algoliasearch from "algoliasearch/lite";
import {
  InstantSearch,
  SearchBox,
  Highlight,
  Snippet,
  connectHits,
  Index,
} from "react-instantsearch-dom";

//https://www.algolia.com/doc/guides/building-search-ui/installation/react/xxwxq

function Search_page(props) {
  let q = props.match.params.q;
  let filter = props.match.params.filter;
  const searchClient = algoliasearch(
    "OZ5CQVIGF7",
    "0297398901312bfad3f0b07d2c906e65"
  );

  const FilterMenu = () => (
    <Container maxWidth="sm">
      <Dropdown>
        <Dropdown.Toggle variant="success" id="dropdown-basic">
          Filter
        </Dropdown.Toggle>

        <Dropdown.Menu>
          <Dropdown.Item href={"/search/q=" + q + "/filter=all"}>
            All
          </Dropdown.Item>
          <Dropdown.Item href={"/search/q=" + q + "/filter=cities"}>
            Cities
          </Dropdown.Item>
          <Dropdown.Item href={"/search/q=" + q + "/filter=organizations"}>
            Organizations
          </Dropdown.Item>
          <Dropdown.Item href={"/search/q=" + q + "/filter=schools"}>
            Schools
          </Dropdown.Item>
        </Dropdown.Menu>
      </Dropdown>
    </Container>
  );

  const CityHits = ({ hits }) => (
    <Container className="cardGrid" maxWidth="md">
      <Grid container spacing={4} maxWidth="md">
        {hits.map((hit) => (
          <Grid item key={hit.id} xs={12} sm={6} md={4}>
            <Card>
              {/* <CardMedia
            className={"cardMedia"}
            image={hit.image}
            title={hit.city}
        /> */}
              <CardContent>
                <Typography gutterBottom variant="h5" component="h2">
                  <Highlight attribute={"city"} tagName="mark" hit={hit} />
                </Typography>
                <Typography align="left">
                  <b>State:&nbsp; </b>
                  <Highlight attribute={"state"} tagName="mark" hit={hit} />
                </Typography>
                <Typography align="left">
                  <b>Population:&nbsp; </b>
                  <Highlight
                    attribute={"totalPopulation"}
                    tagName="mark"
                    hit={hit}
                  />
                </Typography>
                <Typography align="left">
                  <b>Median Income ($):&nbsp; </b>
                  <Highlight
                    attribute={"medianIncome"}
                    tagName="mark"
                    hit={hit}
                  />
                </Typography>
                <Typography align="left">
                  <b>Population in Poverty:&nbsp; </b>
                  <Highlight
                    attribute={"povertyPopulation"}
                    tagName="mark"
                    hit={hit}
                  />
                </Typography>
              </CardContent>
              <CardActions style={{ justifyContent: "center" }}>
                <Button
                  size="small"
                  color="primary"
                  align="center"
                  href={"/locations/" + hit.id}
                >
                  Learn More
                </Button>
              </CardActions>
            </Card>
          </Grid>
        ))}
      </Grid>
    </Container>
  );

  const OrgHits = ({ hits }) => (
    <Container className="cardGrid" maxWidth="md">
      <Grid container spacing={4} maxWidth="md">
        {hits.map((hit) => (
          <Grid item key={hit.id} xs={12} sm={6} md={4}>
            <Card>
              {/* <CardMedia
            className={"cardMedia"}
            image={hit.image}
            title={hit.city}
        /> */}
              <CardContent>
                <Typography gutterBottom variant="h5" component="h2">
                  <Highlight attribute={"name"} tagName="mark" hit={hit} />
                </Typography>
                <Typography align="left">
                  <b>City:&nbsp; </b>
                  <Highlight attribute={"city"} tagName="mark" hit={hit} />
                </Typography>
                <Typography align="left">
                  <b>State:&nbsp;</b>
                  <Highlight attribute={"state"} tagName="mark" hit={hit} />
                </Typography>
                <Typography align="left">
                  <b>Zip:&nbsp;</b>
                  <Highlight attribute={"zip"} tagName="mark" hit={hit} />
                </Typography>
                <Typography align="left">
                  <b>Address:&nbsp;</b>
                  <Highlight attribute={"address"} tagName="mark" hit={hit} />
                </Typography>
                <Typography align="left">
                  <b>IRS Class:&nbsp;</b>
                  <Highlight attribute={"irs_class"} tagName="mark" hit={hit} />
                </Typography>
                <Typography align="left">
                  <b>Cause:&nbsp; </b>
                  <Highlight attribute={"cause"} tagName="mark" hit={hit} />
                </Typography>
                <Typography align="left">
                  <b>Tagline:&nbsp; </b>
                  <Highlight attribute={"tagline"} tagName="mark" hit={hit} />
                </Typography>
                <Typography align="left">
                  <b>Mission:&nbsp; </b>
                  <Snippet attribute={"mission"} tagName="mark" hit={hit} />
                </Typography>
              </CardContent>
              <CardActions style={{ justifyContent: "center" }}>
                <Button
                  size="small"
                  color="primary"
                  align="center"
                  href={"/organizations/" + hit.id}
                >
                  Learn More
                </Button>
              </CardActions>
            </Card>
          </Grid>
        ))}
      </Grid>
    </Container>
  );

  const SchoolHits = ({ hits }) => (
    <Container className="cardGrid" maxWidth="md">
      <Grid container spacing={4} maxWidth="md">
        {hits.map((hit) => (
          <Grid item key={hit.id} xs={12} sm={6} md={4}>
            <Card>
              {/* <CardMedia
            className={"cardMedia"}
            image={hit.image}
            title={hit.city}
        /> */}
              <CardContent>
                <Typography gutterBottom variant="h5" component="h2">
                  <Highlight
                    attribute={"schoolName"}
                    tagName="mark"
                    hit={hit}
                  />
                </Typography>
                <Typography align="left">
                  <b>City:&nbsp; </b>
                  <Highlight attribute={"city"} tagName="mark" hit={hit} />
                </Typography>
                <Typography align="left">
                  <b>State:&nbsp;</b>
                  <Highlight attribute={"state"} tagName="mark" hit={hit} />
                </Typography>
                <Typography align="left">
                  <b>District:&nbsp; </b>
                  <Highlight attribute={"district"} tagName="mark" hit={hit} />
                </Typography>
                <Typography align="left">
                  <b>Level:&nbsp; </b>
                  <Highlight
                    attribute={"schoolLevel"}
                    tagName="mark"
                    hit={hit}
                  />
                </Typography>
                <Typography align="left">
                  <b>Address:&nbsp; </b>
                  <Highlight attribute={"address"} tagName="mark" hit={hit} />
                </Typography>
              </CardContent>
              <CardActions style={{ justifyContent: "center" }}>
                <Button
                  size="small"
                  color="primary"
                  align="center"
                  href={"/schools/" + hit.id}
                >
                  Learn More
                </Button>
              </CardActions>
            </Card>
          </Grid>
        ))}
      </Grid>
    </Container>
  );

  const CustomCityHits = connectHits(CityHits);
  const CustomOrgHits = connectHits(OrgHits);
  const CustomSchoolHits = connectHits(SchoolHits);

  if (filter === "all") {
    return (
      <div className="content">
        <h1>Search Results</h1>
        <h3>
          <em>"{q}"</em>
        </h3>

        <main>
          <FilterMenu />
          {/* https://www.algolia.com/doc/guides/building-search-ui/installation/react/ */}
          <InstantSearch
            searchClient={searchClient}
            indexName="cities"
            searchState={{ query: q }}
          >
            <div style={{ display: "none" }}>
              <SearchBox />
            </div>

            <h3>
              <b>Cities</b>
            </h3>
            <Index indexName="cities">
              <CustomCityHits />
            </Index>
            <br></br>
            <h3>
              <b>Organizations</b>
            </h3>
            <Index indexName="organizations">
              <CustomOrgHits />
            </Index>

            <br></br>
            <h3>
              <b>Schools</b>
            </h3>
            <Index indexName="schools">
              <CustomSchoolHits />
            </Index>
          </InstantSearch>
        </main>
      </div>
    );
  } else if (filter === "cities") {
    return (
      <div className="content">
        <h1>Search Results for Cities</h1>
        <h3>
          <em>"{q}"</em>
        </h3>

        <main>
          <FilterMenu />
          {/* https://www.algolia.com/doc/guides/building-search-ui/installation/react/ */}
          <InstantSearch
            searchClient={searchClient}
            indexName="cities"
            searchState={{ query: q }}
          >
            <div style={{ display: "none" }}>
              <SearchBox />
            </div>
            <Index indexName="cities">
              <CustomCityHits />
            </Index>
            <br></br>
          </InstantSearch>
        </main>
      </div>
    );
  } else if (filter === "organizations") {
    return (
      <div className="content">
        <h1>Search Results for Organizations</h1>
        <h3>
          <em>"{q}"</em>
        </h3>
        <main>
          <FilterMenu />
          {/* https://www.algolia.com/doc/guides/building-search-ui/installation/react/ */}
          <InstantSearch
            searchClient={searchClient}
            indexName="organizations"
            searchState={{ query: q }}
          >
            <div style={{ display: "none" }}>
              <SearchBox />
            </div>
            <Index indexName="organizations">
              <CustomOrgHits />
            </Index>
            <br></br>
          </InstantSearch>
        </main>
      </div>
    );
  } else if (filter === "schools") {
    return (
      <div className="content">
        <h1>Search Results for Schools</h1>
        <h3>
          <em>"{q}"</em>
        </h3>
        <main>
          <FilterMenu />
          {/* https://www.algolia.com/doc/guides/building-search-ui/installation/react/ */}
          <InstantSearch
            searchClient={searchClient}
            indexName="schools"
            searchState={{ query: q }}
          >
            <div style={{ display: "none" }}>
              <SearchBox />
            </div>
            <Index indexName="schools">
              <CustomSchoolHits />
            </Index>
            <br></br>
          </InstantSearch>
        </main>
      </div>
    );
  }
}
export default Search_page;
