// Source: ui material
//https://stackoverflow.com/questions/63705317/usestate-is-not-defined-no-undef-react
// using Burnin Up project and react bootstrap as references
import React, { useEffect, useState } from "react";
import Button from "@material-ui/core/Button";
import Card from "@material-ui/core/Card";
import CardActions from "@material-ui/core/CardActions";
import CardContent from "@material-ui/core/CardContent";
import CardMedia from "@material-ui/core/CardMedia";
import CssBaseline from "@material-ui/core/CssBaseline";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import TableCell from "@material-ui/core/TableCell";
import { makeStyles, useTheme } from "@material-ui/core/styles";
import Container from "@material-ui/core/Container";
import IconButton from "@material-ui/core/IconButton";
import FirstPageIcon from "@material-ui/icons/FirstPage";
import KeyboardArrowLeft from "@material-ui/icons/KeyboardArrowLeft";
import KeyboardArrowRight from "@material-ui/icons/KeyboardArrowRight";
import LastPageIcon from "@material-ui/icons/LastPage";
import TableFooter from "@material-ui/core/TableFooter";
import PropTypes from "prop-types";
import TablePagination from "@material-ui/core/TablePagination";
import TableRow from "@material-ui/core/TableRow";
import { Row, Dropdown, Modal, Col } from "react-bootstrap";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Checkbox from "@material-ui/core/Checkbox";

import TableHead from "@material-ui/core/TableHead";
import TableSortLabel from "@material-ui/core/TableSortLabel";

import axios from "axios";

//https://www.algolia.com/doc/guides/building-search-ui/installation/react/
import { Form, FormControl } from "react-bootstrap";

function FilterMenu() {
  const [nameRange1, setNameRange1] = React.useState("");
  const [nameRange2, setNameRange2] = React.useState("");
  const [nameRange3, setNameRange3] = React.useState("");

  const [stateRange1, setStateRange1] = React.useState("");
  const [stateRange2, setStateRange2] = React.useState("");
  const [stateRange3, setStateRange3] = React.useState("");

  const [popRange1, setpopRange1] = React.useState("");
  const [popRange2, setpopRange2] = React.useState("");
  const [popRange3, setpopRange3] = React.useState("");

  const [incomeRange1, setincomeRange1] = React.useState("");
  const [incomeRange2, setincomeRange2] = React.useState("");
  const [incomeRange3, setincomeRange3] = React.useState("");

  const [povertyRange1, setpovertyRange1] = React.useState("");
  const [povertyRange2, setpovertyRange2] = React.useState("");
  const [povertyRange3, setpovertyRange3] = React.useState("");

  const [show, setShow] = React.useState(false);

  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

  const colStyle = {
    padding: "0.4rem",
  };

  return (
    <Container maxWidth="lg">
      <Button variant="primary" style={{ float: "left" }} onClick={handleShow}>
        Filter
      </Button>
      <Modal show={show} onHide={handleClose} backdrop="static">
        <Modal.Header closeButton>
          <Modal.Title>Filters</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Container fluid>
            <Row className="text-center">
              <Col style={colStyle}>
                <Dropdown>
                  <Dropdown.Toggle size="sm" id="dropdown-basic">
                    Name
                  </Dropdown.Toggle>
                  <Dropdown.Menu>
                    <FormControlLabel
                      control={
                        <Checkbox
                          size="small"
                          name="nameRange1"
                          onChange={(e) => {
                            setNameRange1(e.target.checked ? "a" : "");
                          }}
                        />
                      }
                      label="A-I"
                    />
                    <FormControlLabel
                      control={
                        <Checkbox
                          size="small"
                          name="nameRange2"
                          onChange={(e) => {
                            setNameRange2(e.target.checked ? "j" : "");
                          }}
                        />
                      }
                      label="J-Q"
                    />
                    <FormControlLabel
                      control={
                        <Checkbox
                          size="small"
                          name="nameRange3"
                          onChange={(e) => {
                            setNameRange3(e.target.checked ? "r" : "");
                          }}
                        />
                      }
                      label="R-Z"
                    />
                  </Dropdown.Menu>
                </Dropdown>
              </Col>
              <Col style={colStyle}>
                <Dropdown>
                  <Dropdown.Toggle size="sm" id="dropdown-basic">
                    State
                  </Dropdown.Toggle>
                  <Dropdown.Menu>
                    <FormControlLabel
                      control={
                        <Checkbox
                          size="small"
                          name="stateRange1"
                          onChange={(e) => {
                            setStateRange1(e.target.checked ? "a" : "");
                          }}
                        />
                      }
                      label="A-I"
                    />
                    <FormControlLabel
                      control={
                        <Checkbox
                          size="small"
                          name="stateRange2"
                          onChange={(e) => {
                            setStateRange2(e.target.checked ? "j" : "");
                          }}
                        />
                      }
                      label="J-Q"
                    />
                    <FormControlLabel
                      control={
                        <Checkbox
                          size="small"
                          name="stateRange3"
                          onChange={(e) => {
                            setStateRange3(e.target.checked ? "r" : "");
                          }}
                        />
                      }
                      label="R-Z"
                    />
                  </Dropdown.Menu>
                </Dropdown>
              </Col>
              <Col style={colStyle}>
                <Dropdown>
                  <Dropdown.Toggle size="sm" id="dropdown-basic">
                    Population
                  </Dropdown.Toggle>
                  <Dropdown.Menu>
                    <FormControlLabel
                      control={
                        <Checkbox
                          size="small"
                          name="categoryRange1"
                          onChange={(e) => {
                            setpopRange1(e.target.checked ? "1" : "");
                          }}
                        />
                      }
                      label="0-250,000"
                    />
                    <FormControlLabel
                      control={
                        <Checkbox
                          size="small"
                          name="categoryRange2"
                          onChange={(e) => {
                            setpopRange2(e.target.checked ? "2" : "");
                          }}
                        />
                      }
                      label="250,001-600,000"
                    />
                    <FormControlLabel
                      control={
                        <Checkbox
                          size="small"
                          name="categoryRange3"
                          onChange={(e) => {
                            setpopRange3(e.target.checked ? "3" : "");
                          }}
                        />
                      }
                      label="600,001+"
                    />
                  </Dropdown.Menu>
                </Dropdown>
              </Col>
              <Col style={colStyle}>
                <Dropdown>
                  <Dropdown.Toggle size="sm" id="dropdown-basic">
                    Poverty
                  </Dropdown.Toggle>
                  <Dropdown.Menu>
                    <FormControlLabel
                      control={
                        <Checkbox
                          size="small"
                          name="zipRange1"
                          onChange={(e) => {
                            setpovertyRange1(e.target.checked ? "1" : "");
                          }}
                        />
                      }
                      label="0-40,000"
                    />
                    <FormControlLabel
                      control={
                        <Checkbox
                          size="small"
                          name="zipRange2"
                          onChange={(e) => {
                            setpovertyRange2(e.target.checked ? "2" : "");
                          }}
                        />
                      }
                      label="40,001-90,000"
                    />
                    <FormControlLabel
                      control={
                        <Checkbox
                          size="small"
                          name="zipRange3"
                          onChange={(e) => {
                            setpovertyRange3(e.target.checked ? "3" : "");
                          }}
                        />
                      }
                      label="90,000+"
                    />
                  </Dropdown.Menu>
                </Dropdown>
              </Col>
              <Col style={colStyle}>
                <Dropdown>
                  <Dropdown.Toggle size="sm" id="dropdown-basic">
                    Median Income
                  </Dropdown.Toggle>
                  <Dropdown.Menu>
                    <FormControlLabel
                      control={
                        <Checkbox
                          size="small"
                          name="incomeRange1"
                          onChange={(e) => {
                            setincomeRange1(e.target.checked ? "1" : "");
                          }}
                        />
                      }
                      label="0-50,000"
                    />
                    <FormControlLabel
                      control={
                        <Checkbox
                          size="small"
                          name="incomeRange2"
                          onChange={(e) => {
                            setincomeRange2(e.target.checked ? "2" : "");
                          }}
                        />
                      }
                      label="50,001-80,000"
                    />
                    <FormControlLabel
                      control={
                        <Checkbox
                          size="small"
                          name="incomeRange3"
                          onChange={(e) => {
                            setincomeRange3(e.target.checked ? "3" : "");
                          }}
                        />
                      }
                      label="80,001+"
                    />
                  </Dropdown.Menu>
                </Dropdown>
              </Col>
            </Row>
          </Container>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={handleClose}>
            Close
          </Button>
          <Button
            variant="primary"
            onClick={(e) => {
              window.location.href =
                "/locations/n=" +
                nameRange1 +
                nameRange2 +
                nameRange3 +
                "/s=" +
                stateRange1 +
                stateRange2 +
                stateRange3 +
                "/po=" +
                popRange1 +
                popRange2 +
                popRange3 +
                "/pv=" +
                povertyRange1 +
                povertyRange2 +
                povertyRange3 +
                "/i=" +
                incomeRange1 +
                incomeRange2 +
                incomeRange3;
            }}
          >
            Apply
          </Button>
        </Modal.Footer>
      </Modal>
      <br></br>
    </Container>
  );
}

/*********************************************************************************/
/******************************** GRID STYLING ***********************************/
/*********************************************************************************/

const useStyles = makeStyles((theme) => ({
  icon: {
    marginRight: theme.spacing(2),
  },
  heroContent: {
    backgroundColor: theme.palette.background.paper,
    padding: theme.spacing(8, 0, 6),
  },
  heroButtons: {
    marginTop: theme.spacing(4),
    justifyContent: "center",
  },
  cardGrid: {
    paddingTop: theme.spacing(8),
    paddingBottom: theme.spacing(8),
  },
  card: {
    height: "100%",
    display: "flex",
    flexDirection: "column",
  },
  cardMedia: {
    paddingTop: "56.25%", // 16:9
  },
  cardContent: {
    flexGrow: 1,
  },
}));

/*********************************************************************************/
/******************************** PAGING CONTROLS ********************************/
/*********************************************************************************/

const useStyles1 = makeStyles((theme) => ({
  root: {
    flexShrink: 0,
    marginLeft: theme.spacing(2.5),
  },
}));

function TablePaginationActions(props) {
  const classes = useStyles1();
  const theme = useTheme();
  const { count, page, rowsPerPage, onChangePage } = props;

  const handleFirstPageButtonClick = (event) => {
    onChangePage(event, 0);
  };

  const handleBackButtonClick = (event) => {
    onChangePage(event, page - 1);
  };

  const handleNextButtonClick = (event) => {
    onChangePage(event, page + 1);
  };

  const handleLastPageButtonClick = (event) => {
    onChangePage(event, Math.max(0, Math.ceil(count / rowsPerPage) - 1));
  };

  return (
    <div className={classes.root}>
      <IconButton
        onClick={handleFirstPageButtonClick}
        disabled={page === 0}
        aria-label="first page"
      >
        {theme.direction === "rtl" ? <LastPageIcon /> : <FirstPageIcon />}
      </IconButton>
      <IconButton
        onClick={handleBackButtonClick}
        disabled={page === 0}
        aria-label="previous page"
      >
        {theme.direction === "rtl" ? (
          <KeyboardArrowRight />
        ) : (
          <KeyboardArrowLeft />
        )}
      </IconButton>
      <IconButton
        onClick={handleNextButtonClick}
        disabled={page >= Math.ceil(count / rowsPerPage) - 1}
        aria-label="next page"
      >
        {theme.direction === "rtl" ? (
          <KeyboardArrowLeft />
        ) : (
          <KeyboardArrowRight />
        )}
      </IconButton>
      <IconButton
        onClick={handleLastPageButtonClick}
        disabled={page >= Math.ceil(count / rowsPerPage) - 1}
        aria-label="last page"
      >
        {theme.direction === "rtl" ? <FirstPageIcon /> : <LastPageIcon />}
      </IconButton>
    </div>
  );
}

TablePaginationActions.propTypes = {
  count: PropTypes.number.isRequired,
  onChangePage: PropTypes.func.isRequired,
  page: PropTypes.number.isRequired,
  rowsPerPage: PropTypes.number.isRequired,
};

/*********************************************************************************/
/******************************** SORTING CONTROLS *******************************/
/*********************************************************************************/

function getComparator(order, orderBy) {
  return order === "desc"
    ? (a, b) => descendingComparator(a, b, orderBy)
    : (a, b) => -descendingComparator(a, b, orderBy);
}

function stableSort(array, comparator) {
  const stabilizedThis = array.map((el, index) => [el, index]);
  stabilizedThis.sort((a, b) => {
    const order = comparator(a[0], b[0]);
    if (order !== 0) return order;
    return a[1] - b[1];
  });
  return stabilizedThis.map((el) => el[0]);
}

EnhancedTableHead.propTypes = {
  classes: PropTypes.object.isRequired,
  onRequestSort: PropTypes.func.isRequired,
  order: PropTypes.oneOf(["asc", "desc"]).isRequired,
  orderBy: PropTypes.string.isRequired,
  rowCount: PropTypes.number.isRequired,
};

function descendingComparator(a, b, orderBy) {
  if (b[orderBy] < a[orderBy]) {
    return -1;
  }
  if (b[orderBy] > a[orderBy]) {
    return 1;
  }
  return 0;
}

// Sort Labels
const headCells = [
  { id: "city", numeric: false, disablePadding: false, label: "City Name " },
  { id: "state", numeric: false, disablePadding: false, label: "State " },
  {
    id: "totalPopulation",
    numeric: true,
    disablePadding: false,
    label: "Population ",
  },
  {
    id: "medianIncome",
    numeric: true,
    disablePadding: false,
    label: "Median Income ($) ",
  },
  {
    id: "povertyPopulation",
    numeric: true,
    disablePadding: false,
    label: "Population in Poverty ",
  },
];

function EnhancedTableHead(props) {
  const { classes, order, orderBy, onRequestSort } = props;
  const createSortHandler = (property) => (event) => {
    onRequestSort(event, property);
  };

  return (
    <TableHead>
      <TableRow>
        {headCells.map((headCell) => (
          <TableCell
            key={headCell.id}
            align={headCell.numeric ? "left" : "left"}
            padding={headCell.disablePadding ? "none" : "default"}
            sortDirection={orderBy === headCell.id ? order : false}
          >
            <TableSortLabel
              active={orderBy === headCell.id}
              direction={orderBy === headCell.id ? order : "asc"}
              onClick={createSortHandler(headCell.id)}
            >
              {headCell.label}
              {orderBy === headCell.id ? (
                <span className={classes.visuallyHidden}>
                  {/*order === 'desc' ? 'sorted descending' : 'sorted ascending'*/}
                </span>
              ) : null}
            </TableSortLabel>
          </TableCell>
        ))}
      </TableRow>
    </TableHead>
  );
}

EnhancedTableHead.propTypes = {
  classes: PropTypes.object.isRequired,
  onRequestSort: PropTypes.func.isRequired,
  order: PropTypes.oneOf(["asc", "desc"]).isRequired,
  orderBy: PropTypes.string.isRequired,
  rowCount: PropTypes.number.isRequired,
};

/*********************************************************************************/
/******************************** CARDS OUTPUT FN ********************************/
/*********************************************************************************/

function CityModel(props) {
  // https://reactjs.org/docs/faq-ajax.html
  const [error] = useState(null);
  const [isLoaded, setIsLoaded] = useState(false);
  const [items, setItems] = useState([]);
  const [order, setOrder] = React.useState("asc");
  const [orderBy, setOrderBy] = React.useState("");

  const input = React.useRef();

  // Note: the empty deps array [] means
  // this useEffect will run once
  // similar to componentDidMount()
  const name =
    props.match.params.name !== undefined ? props.match.params.name : "";
  const state =
    props.match.params.state !== undefined ? props.match.params.state : "";
  const population =
    props.match.params.population !== undefined
      ? props.match.params.population
      : "";
  const poverty =
    props.match.params.poverty !== undefined ? props.match.params.poverty : "";
  const income =
    props.match.params.income !== undefined ? props.match.params.income : "";

  useEffect(() => {
    const fetchPosts = async () => {
      setIsLoaded(false);
      let url =
        "https://api.hopeforhomeless.me/cities?fcity=" +
        name +
        "&fstate=" +
        state +
        "&fpopulation=" +
        population +
        "&fmedianincome=" +
        income +
        "&fpoverty=" +
        poverty;
      const res = await axios.get(url);
      const info = res["data"];
      setItems(info);
      setIsLoaded(true);
    };

    fetchPosts();
  }, [income, name, population, poverty, state]);

  const classes = useStyles();

  // for paging
  const [page, setPage] = React.useState(0);
  const [rowsPerPage, setRowsPerPage] = React.useState(6);

  const emptyRows =
    rowsPerPage - Math.min(rowsPerPage, items.length - page * rowsPerPage);

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };

  const handleRequestSort = (event, property) => {
    const isAsc = orderBy === property && order === "asc";
    setOrder(isAsc ? "desc" : "asc");
    setOrderBy(property);
  };

  if (error) {
    return <div>Error: {error.message}</div>;
  } else if (!isLoaded) {
    return <div>Loading...</div>;
  } else {
    return (
      <React.Fragment>
        <CssBaseline />
        <main>
          <br></br>
          <Container className={classes.cardGrid} maxWidth="md">
            <Typography
              component="h5"
              variant="h5"
              align="center"
              color="textPrimary"
              gutterBottom
            >
              Cities
            </Typography>
            {/* https://www.algolia.com/doc/guides/building-search-ui/installation/react/ */}
            <Form
              className="justify-content-center"
              inline
              onSubmit={(e) => {
                e.preventDefault();
              }}
            >
              <FormControl
                type="text"
                placeholder="Search"
                className="mr-sm-2"
                ref={input}
                onKeyPress={(event) => {
                  if (event.key === "Enter") {
                    window.location.href =
                      "/search/q=" + input.current.value + "/filter=cities";
                  }
                }}
              />
              {/* <Button variant="outline-success" onClick={() => this.search()}>Search</Button> */}
            </Form>
            <FilterMenu />

            {/* Sort Mechanisms */}
            <EnhancedTableHead
              classes={classes}
              order={order}
              orderBy={orderBy}
              onRequestSort={handleRequestSort}
              rowCount={items.length}
            />

            <br></br>
            <Container className="cardGrid">
              <Grid container spacing={4}>
                {/* {(rowsPerPage > 0
                        ? items.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                        : items
                            ).map((item) => ( */}

                {(rowsPerPage > 0
                  ? stableSort(items, getComparator(order, orderBy)).slice(
                      page * rowsPerPage,
                      page * rowsPerPage + rowsPerPage
                    )
                  : stableSort(items, getComparator(order, orderBy))
                ).map((item) => (
                  <Grid item key={items.indexOf(item)} xs={12} sm={6} md={4}>
                    <Card className={classes.card}>
                      <CardMedia
                        className={classes.cardMedia}
                        image={item.image}
                        title={item.city}
                      />
                      <CardContent className={classes.cardContent}>
                        <Typography gutterBottom variant="h5" component="h2">
                          {item.city}
                        </Typography>
                        <Typography>State: {item.state}</Typography>
                        <Typography>
                          Population: {item.totalPopulation}
                        </Typography>
                        <Typography>
                          Median Income ($): {item.medianIncome}
                        </Typography>
                        <Typography>
                          Population in Poverty: {item.povertyPopulation}
                        </Typography>
                      </CardContent>
                      <CardActions style={{ justifyContent: "center" }}>
                        <Button
                          size="small"
                          color="primary"
                          align="center"
                          href={"/locations/" + item.id}
                        >
                          Learn More
                        </Button>
                      </CardActions>
                    </Card>
                  </Grid>
                ))}
                {emptyRows > 0 && (
                  <TableRow style={{ height: 53 * emptyRows }}>
                    <TableCell colSpan={6} />
                  </TableRow>
                )}
              </Grid>
            </Container>
            <TableFooter>
              <TablePagination
                rowsPerPageOptions={[6, 9, 12, { label: "All", value: -1 }]}
                colSpan={3}
                count={items.length}
                rowsPerPage={rowsPerPage}
                page={page}
                SelectProps={{
                  inputProps: { "aria-label": "rows per page" },
                  native: true,
                }}
                onChangePage={handleChangePage}
                onChangeRowsPerPage={handleChangeRowsPerPage}
                ActionsComponent={TablePaginationActions}
              ></TablePagination>
            </TableFooter>
          </Container>
        </main>
      </React.Fragment>
    );
  }
}
export default CityModel;
