//https://stackoverflow.com/questions/63705317/usestate-is-not-defined-no-undef-react
import React, { useEffect, useState } from "react";
import "../style.css";
import "../App.css";

import Typography from "@material-ui/core/Typography";
import PostsSchool from "./SchoolModelPage/Posts_School";
import PostsOrgs from "./OrgModelPage/Posts_Org";

import { makeStyles } from "@material-ui/core/styles";
import Paper from "@material-ui/core/Paper";
import Grid from "@material-ui/core/Grid";

function Location_instance(props) {
  //https://reactjs.org/docs/faq-ajax.html

  const [items, setItems] = useState([]);

  const [items2, setItems2] = useState([]);

  const [items3, setItems3] = useState([]);

  let id = props.match.params.id;
  // Note: the empty deps array [] means
  // this useEffect will run once
  // similar to componentDidMount()
  useEffect(() => {
    fetch("https://api.hopeforhomeless.me/cities/" + id)
      .then((res) => res.json())
      .then((result) => {
        setItems(result);
      });
  });

  useEffect(() => {
    fetch("https://api.hopeforhomeless.me/organizations?cityId=" + id)
      .then((res) => res.json())
      .then((result) => {
        setItems2(result);
      });
  });

  useEffect(() => {
    fetch("https://api.hopeforhomeless.me/schools?cityId=" + id)
      .then((res) => res.json())
      .then((result) => {
        setItems3(result);
      });
  });

  const useStyles = makeStyles((theme) => ({
    root: {
      flexGrow: 1,
    },
    paper: {
      padding: theme.spacing(1),
      textAlign: "center",
      color: theme.palette.text.primary,
    },
  }));

  const classes = useStyles();

  return (
    <div className="Location_instance">
      <body>
        <main>
          <div class="content">
            <div class="cityDescription">
              <div class="cityPage">
                <span class="title">
                  <span class="ant-typography" styles="font-size: 24px;"></span>
                </span>

                <div class="cityDescription">
                  <h2 class="ant-typography" styles="text-align: center;">
                    {items.city}
                  </h2>

                  <img
                    src={items.image}
                    width="500"
                    height="400"
                    alt="City_1_IMG"
                    class="cityImage"
                  />

                  <div className={classes.root}>
                      <Grid container spacing={2}>
                          <Grid item xs={6}>
                            <Paper className={classes.paper}><strong>State:</strong></Paper>
                          </Grid>
                          <Grid item xs={6}>
                            <Paper className={classes.paper}>{items.state}</Paper>
                          </Grid>

                          <Grid item xs={6}>
                            <Paper className={classes.paper}><strong>Total Population:</strong></Paper>
                          </Grid>
                          <Grid item xs={6}>
                            <Paper className={classes.paper}>{items.totalPopulation}</Paper>
                          </Grid>

                          <Grid item xs={6}>
                            <Paper className={classes.paper}><strong>Population Living in Poverty:</strong></Paper>
                          </Grid>
                          <Grid item xs={6}>
                            <Paper className={classes.paper}>{items.povertyPopulation}</Paper>
                          </Grid>

                          <Grid item xs={6}>
                            <Paper className={classes.paper}><strong>Portion of Population with Health Insurance:</strong></Paper>
                          </Grid>
                          <Grid item xs={6}>
                            <Paper className={classes.paper}>{items.percentHealthInsured}%</Paper>
                          </Grid>

                          <Grid item xs={6}>
                            <Paper className={classes.paper}><strong>Percentage of Housing that is Vacant:</strong></Paper>
                          </Grid>
                          <Grid item xs={6}>
                            <Paper className={classes.paper}>{items.vacantHousingPercent}%</Paper>
                          </Grid>
                        </Grid>
                  </div>

                  <div class="ant-typography">Map of {items.city}</div>

                  <iframe
                    title="City map"
                    src={items.map}
                    width="600"
                    height="450"
                    styles="border:0;"
                    allowfullscreen=""
                    loading="lazy"
                  ></iframe>

                  {/* <article class="cityDetails"> */}

                  <Grid container spacing={2}>
                    <Grid item xs={6}>
                      <Paper className={classes.paper}>
                        <strong>Median Rent Price:</strong>
                      </Paper>
                    </Grid>
                    <Grid item xs={6}>
                      <Paper className={classes.paper}>
                        A 1 bedroom apartment in this city is $
                        {items.medianRent}
                      </Paper>
                    </Grid>

                    <Grid item xs={6}>
                      <Paper className={classes.paper}>
                        <strong>
                          Percentage of Population on Food Stamps:
                        </strong>
                      </Paper>
                    </Grid>
                    <Grid item xs={6}>
                      <Paper className={classes.paper}>
                        {items.foodStampsPercent}%
                      </Paper>
                    </Grid>

                    <Grid item xs={6}>
                      <Paper className={classes.paper}>
                        <strong>
                          Percentage of Population with a College Degree:
                        </strong>
                      </Paper>
                    </Grid>
                    <Grid item xs={6}>
                      <Paper className={classes.paper}>
                        {items.collegeDegreePercent}%
                      </Paper>
                    </Grid>
                  </Grid>

                  <article class="cityOtherDetails">
                    <Typography
                      component="h5"
                      variant="h5"
                      align="center"
                      color="textPrimary"
                      gutterBottom
                    >
                      More in the Area{"\n"}
                    </Typography>

                    {/* SCHOOLS TABLE */}
                    <React.Fragment>
                      <Typography
                        component="h5"
                        variant="h5"
                        align="left"
                        color="textPrimary"
                        gutterBottom
                      >
                        {"\n"}
                      </Typography>
                      <PostsSchool
                        posts={items3}
                        loading={null}
                        totalPosts={items3.length}
                      />
                    </React.Fragment>

                    {/* ORG TABLE */}
                    <React.Fragment>
                      <Typography
                        component="h5"
                        variant="h5"
                        align="left"
                        color="textPrimary"
                        gutterBottom
                      >
                        {"\n"}
                      </Typography>
                      <PostsOrgs
                        posts={items2}
                        loading={null}
                        totalPosts={items2.length}
                      />
                    </React.Fragment>
                  </article>
                </div>
              </div>
            </div>
          </div>
        </main>

        <footer></footer>

        <script
          src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/js/bootstrap.bundle.min.js"
          integrity="sha384-b5kHyXgcpbZJO/tY9Ul7kGkf1S0CWuKcCD38l8YkeH8z8QjE0GmW1gYU5S9FOnJ0"
          crossorigin="anonymous"
        ></script>
      </body>
    </div>
  );
}

export default Location_instance;
