// Source: ui material
import React, { useEffect, useState } from "react";
import Typography from "@material-ui/core/Typography";
import PostsOrgs from "./OrgModelPage/Posts_Org";

import { makeStyles } from "@material-ui/core/styles";
import Paper from "@material-ui/core/Paper";
import Grid from "@material-ui/core/Grid";

import "../style.css";
import "../App.css";

function School_instance(props) {
  //https://reactjs.org/docs/faq-ajax.html
  const [items, setItems] = useState([]);
  const [items2, setItems2] = useState([]);

  let id = props.match.params.id;

  useEffect(() => {
    fetch("https://api.hopeforhomeless.me/schools/" + id)
      .then((res) => res.json())
      .then((result) => {
        setItems(result);
        fetch(
          "https://api.hopeforhomeless.me/organizations?cityId=" + result.cityId
        )
          .then((res) => res.json())
          .then((result) => {
            setItems2(result);
          });
      });
  });

  const useStyles = makeStyles((theme) => ({
    root: {
      flexGrow: 1,
    },
    paper: {
      padding: theme.spacing(1),
      textAlign: "center",
      color: theme.palette.text.primary,
    },
  }));

  const classes = useStyles();
  return (
    <div className="Location_instance">
      <body>
        <main>
          <div className="content">
            <div className="cityDescription">
              <div className="cityPage">
                <span className="title">
                  <span
                    className="ant-typography"
                    styles="font-size: 24px;"
                  ></span>
                </span>

                <div className="cityDescription">
                  <h2 className="ant-typography" styles="text-align: center;">
                    {items.schoolName}
                  </h2>

                  {/* Embedded Video */}

                  <iframe
                    title="School video"
                    width="600"
                    height="450"
                    styles="border:0;"
                    src={items.video}
                  ></iframe>

                  <div className={classes.root}>
                    <Grid container spacing={2}>
                      <Grid item xs={6} m={2}>
                        <Paper className={classes.paper}>
                          <strong>School Name</strong>
                        </Paper>
                      </Grid>
                      <Grid item xs={6}>
                        <Paper className={classes.paper}>
                          {items.schoolName}
                        </Paper>
                      </Grid>

                      <Grid item xs={6}>
                        <Paper className={classes.paper}>
                          <strong>State:</strong>
                        </Paper>
                      </Grid>
                      <Grid item xs={6}>
                        <Paper className={classes.paper}>{items.state}</Paper>
                      </Grid>

                      <Grid item xs={6}>
                        <Paper className={classes.paper}>
                          <strong>Address:</strong>
                        </Paper>
                      </Grid>
                      <Grid item xs={6}>
                        <Paper className={classes.paper}>{items.address}</Paper>
                      </Grid>

                      <Grid item xs={6}>
                        <Paper className={classes.paper}>
                          <strong>School District:</strong>
                        </Paper>
                      </Grid>
                      <Grid item xs={6}>
                        <Paper className={classes.paper}>
                          {items.district}
                        </Paper>
                      </Grid>

                      <Grid item xs={6}>
                        <Paper className={classes.paper}>
                          <strong>Rank Percentile:</strong>
                        </Paper>
                      </Grid>
                      <Grid item xs={6}>
                        <Paper className={classes.paper}>
                          {items.rankPercentile}%
                        </Paper>
                      </Grid>
                    </Grid>
                  </div>

                  <div class="ant-typography">Map of {items.schoolName}</div>

                  <iframe
                    title="School map"
                    src={items.map}
                    width="600"
                    height="450"
                    styles="border:0;"
                    allowfullscreen=""
                    loading="lazy"
                  ></iframe>
                </div>

                <div className={classes.root}>
                  <Grid container spacing={2}>
                    <Grid item xs={6}>
                      <Paper className={classes.paper}>
                        <strong>Number of Students:</strong>
                      </Paper>
                    </Grid>
                    <Grid item xs={6}>
                      <Paper className={classes.paper}>
                        {items.numStudents}
                      </Paper>
                    </Grid>

                    <Grid item xs={6}>
                      <Paper className={classes.paper}>
                        <strong>Percentage of Students on Free Lunch:</strong>
                      </Paper>
                    </Grid>
                    <Grid item xs={6}>
                      <Paper className={classes.paper}>
                        {items.percentFreeLunch}%
                      </Paper>
                    </Grid>

                    <Grid item xs={6}>
                      <Paper className={classes.paper}>
                        <strong>Student to Teacher Ratio:</strong>
                      </Paper>
                    </Grid>
                    <Grid item xs={6}>
                      <Paper className={classes.paper}>
                        {items.studentTeacherRatio}
                      </Paper>
                    </Grid>

                    <Grid item xs={6}>
                      <Paper className={classes.paper}>
                        <strong>Latitude:</strong>
                      </Paper>
                    </Grid>
                    <Grid item xs={6}>
                      <Paper className={classes.paper}>{items.latitude}</Paper>
                    </Grid>

                    <Grid item xs={6}>
                      <Paper className={classes.paper}>
                        <strong>Longitude</strong>
                      </Paper>
                    </Grid>
                    <Grid item xs={6}>
                      <Paper className={classes.paper}>{items.longitude}</Paper>
                    </Grid>
                  </Grid>
                </div>

                <Typography
                  component="h5"
                  variant="h5"
                  align="left"
                  color="textPrimary"
                  gutterBottom
                >
                  {"\n"}
                </Typography>
                <Typography
                  component="h5"
                  variant="h5"
                  color="textPrimary"
                  gutterBottom
                >
                  Learn more about this city:
                </Typography>
                <Grid item xs={12}>
                  <div>
                    <a href={"/locations/" + items.cityId}>{items.city}</a>
                  </div>
                </Grid>

                {/* ORG TABLE */}
                <React.Fragment>
                  <Typography
                    component="h5"
                    variant="h5"
                    align="left"
                    color="textPrimary"
                    gutterBottom
                  >
                    {"\n"}
                  </Typography>
                  <PostsOrgs
                    posts={items2}
                    loading={null}
                    totalPosts={items2.length}
                  />
                </React.Fragment>
              </div>
            </div>
          </div>
        </main>

        <footer></footer>

        <script
          src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/js/bootstrap.bundle.min.js"
          integrity="sha384-b5kHyXgcpbZJO/tY9Ul7kGkf1S0CWuKcCD38l8YkeH8z8QjE0GmW1gYU5S9FOnJ0"
          crossorigin="anonymous"
        ></script>
      </body>
    </div>
  );
}

export default School_instance;
