// Source:

// https://github.com/bradtraversy/simple_react_pagination
// https://www.youtube.com/watch?v=IYCa1F-OWmk

import React, { useState, useEffect } from "react";
import Posts from "./Posts_School";
import axios from "axios";
import Typography from "@material-ui/core/Typography";
import Container from "@material-ui/core/Container";

const APICallTest = (props) => {
  const [posts, setPosts] = useState([]);
  const [loading, setLoading] = useState(false);
  //const [currentPage, setCurrentPage] = useState(1);
  //const [postsPerPage] = useState(10);

  const name =
    props.match.params.name !== undefined ? props.match.params.name : "";
  const state =
    props.match.params.state !== undefined ? props.match.params.state : "";
  const district =
    props.match.params.district !== undefined
      ? props.match.params.district
      : "";
  const level =
    props.match.params.level !== undefined ? props.match.params.level : "";
  const rank =
    props.match.params.rank !== undefined ? props.match.params.rank : "";

  useEffect(
    (/*name, state, district, level, rank*/) => {
      const fetchPosts = async () => {
        setLoading(true);
        let url =
          "https://api.hopeforhomeless.me/schools?fname=" +
          name +
          "&fstate=" +
          state +
          "&fdistrict=" +
          district +
          "&flevel=" +
          level +
          "&frank=" +
          rank;
        const res = await axios.get(url);
        const info = res["data"];
        setPosts(info);
        setLoading(false);
      };

      fetchPosts();
    },
    [name, state, district, level, rank]
  );

  return (
    <React.Fragment>
      <Container>
        <Typography
          component="h2"
          variant="h2"
          align="center"
          color="textPrimary"
          gutterBottom
        >
          Schools
        </Typography>
        <Posts posts={posts} loading={loading} totalPosts={posts.length} />
      </Container>
    </React.Fragment>
  );
};

export default APICallTest;
