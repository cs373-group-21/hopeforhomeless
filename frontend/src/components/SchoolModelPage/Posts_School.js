// Sources:

// https://github.com/bradtraversy/simple_react_pagination
// https://www.youtube.com/watch?v=IYCa1F-OWmk
// https://material-ui.com/components/tables/
// using Burnin Up project and react bootstrap as references

import React from "react";
import { withStyles, makeStyles, useTheme } from "@material-ui/core/styles";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Paper from "@material-ui/core/Paper";
import Typography from "@material-ui/core/Typography";
import IconButton from "@material-ui/core/IconButton";
import FirstPageIcon from "@material-ui/icons/FirstPage";
import KeyboardArrowLeft from "@material-ui/icons/KeyboardArrowLeft";
import KeyboardArrowRight from "@material-ui/icons/KeyboardArrowRight";
import LastPageIcon from "@material-ui/icons/LastPage";
import TableFooter from "@material-ui/core/TableFooter";
import PropTypes from "prop-types";
import TablePagination from "@material-ui/core/TablePagination";
import Container from "@material-ui/core/Container";
import { Form, FormControl } from "react-bootstrap";
import Checkbox from "@material-ui/core/Checkbox";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import TableSortLabel from "@material-ui/core/TableSortLabel";
import { Row, Dropdown, Modal, Col } from "react-bootstrap";
import Button from "@material-ui/core/Button";

function FilterMenu() {
  const [nameRange1, setNameRange1] = React.useState("");
  const [nameRange2, setNameRange2] = React.useState("");
  const [nameRange3, setNameRange3] = React.useState("");

  const [stateRange1, setStateRange1] = React.useState("");
  const [stateRange2, setStateRange2] = React.useState("");
  const [stateRange3, setStateRange3] = React.useState("");

  const [districtRange1, setdistrictRange1] = React.useState("");
  const [districtRange2, setdistrictRange2] = React.useState("");
  const [districtRange3, setdistrictRange3] = React.useState("");

  const [levelRange1, setlevelRange1] = React.useState("");
  const [levelRange2, setlevelRange2] = React.useState("");
  const [levelRange3, setlevelRange3] = React.useState("");

  const [rankRange1, setrankRange1] = React.useState("");
  const [rankRange2, setrankRange2] = React.useState("");
  const [rankRange3, setrankRange3] = React.useState("");

  const [show, setShow] = React.useState(false);

  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

  const colStyle = {
    padding: "0.4rem",
  };

  return (
    <Container maxWidth="lg">
      <Button style={{ float: "left" }} onClick={handleShow}>
        Filter
      </Button>
      <Modal show={show} onHide={handleClose} backdrop="static">
        <Modal.Header closeButton>
          <Modal.Title>Filters</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Container fluid>
            <Row className="text-center">
              <Col style={colStyle}>
                <Dropdown>
                  <Dropdown.Toggle size="sm" id="dropdown-basic">
                    Name
                  </Dropdown.Toggle>
                  <Dropdown.Menu>
                    <FormControlLabel
                      control={
                        <Checkbox
                          size="small"
                          name="nameRange1"
                          onChange={(e) => {
                            setNameRange1(e.target.checked ? "a" : "");
                          }}
                        />
                      }
                      label="A-I"
                    />
                    <FormControlLabel
                      control={
                        <Checkbox
                          size="small"
                          name="nameRange2"
                          onChange={(e) => {
                            setNameRange2(e.target.checked ? "j" : "");
                          }}
                        />
                      }
                      label="J-Q"
                    />
                    <FormControlLabel
                      control={
                        <Checkbox
                          size="small"
                          name="nameRange3"
                          onChange={(e) => {
                            setNameRange3(e.target.checked ? "r" : "");
                          }}
                        />
                      }
                      label="R-Z"
                    />
                  </Dropdown.Menu>
                </Dropdown>
              </Col>
              <Col style={colStyle}>
                <Dropdown>
                  <Dropdown.Toggle size="sm" id="dropdown-basic">
                    State
                  </Dropdown.Toggle>
                  <Dropdown.Menu>
                    <FormControlLabel
                      control={
                        <Checkbox
                          size="small"
                          name="stateRange1"
                          onChange={(e) => {
                            setStateRange1(e.target.checked ? "a" : "");
                          }}
                        />
                      }
                      label="A-I"
                    />
                    <FormControlLabel
                      control={
                        <Checkbox
                          size="small"
                          name="stateRange2"
                          onChange={(e) => {
                            setStateRange2(e.target.checked ? "j" : "");
                          }}
                        />
                      }
                      label="J-Q"
                    />
                    <FormControlLabel
                      control={
                        <Checkbox
                          size="small"
                          name="stateRange3"
                          onChange={(e) => {
                            setStateRange3(e.target.checked ? "r" : "");
                          }}
                        />
                      }
                      label="R-Z"
                    />
                  </Dropdown.Menu>
                </Dropdown>
              </Col>
              <Col style={colStyle}>
                <Dropdown>
                  <Dropdown.Toggle size="sm" id="dropdown-basic">
                    District
                  </Dropdown.Toggle>
                  <Dropdown.Menu>
                    <FormControlLabel
                      control={
                        <Checkbox
                          size="small"
                          name="districtRange1"
                          onChange={(e) => {
                            setdistrictRange1(e.target.checked ? "a" : "");
                          }}
                        />
                      }
                      label="A-I"
                    />
                    <FormControlLabel
                      control={
                        <Checkbox
                          size="small"
                          name="districtRange2"
                          onChange={(e) => {
                            setdistrictRange2(e.target.checked ? "j" : "");
                          }}
                        />
                      }
                      label="J-Q"
                    />
                    <FormControlLabel
                      control={
                        <Checkbox
                          size="small"
                          name="districtRange3"
                          onChange={(e) => {
                            setdistrictRange3(e.target.checked ? "r" : "");
                          }}
                        />
                      }
                      label="R-Z"
                    />
                  </Dropdown.Menu>
                </Dropdown>
              </Col>
              <Col style={colStyle}>
                <Dropdown>
                  <Dropdown.Toggle size="sm" id="dropdown-basic">
                    Level
                  </Dropdown.Toggle>
                  <Dropdown.Menu>
                    <FormControlLabel
                      control={
                        <Checkbox
                          size="small"
                          name="zipRange1"
                          onChange={(e) => {
                            setlevelRange1(e.target.checked ? "e" : "");
                          }}
                        />
                      }
                      label="Elementary"
                    />
                    <FormControlLabel
                      control={
                        <Checkbox
                          size="small"
                          name="zipRange2"
                          onChange={(e) => {
                            setlevelRange2(e.target.checked ? "m" : "");
                          }}
                        />
                      }
                      label="Middle"
                    />
                    <FormControlLabel
                      control={
                        <Checkbox
                          size="small"
                          name="zipRange3"
                          onChange={(e) => {
                            setlevelRange3(e.target.checked ? "h" : "");
                          }}
                        />
                      }
                      label="High"
                    />
                  </Dropdown.Menu>
                </Dropdown>
              </Col>
              <Col style={colStyle}>
                <Dropdown>
                  <Dropdown.Toggle size="sm" id="dropdown-basic">
                    Rank
                  </Dropdown.Toggle>
                  <Dropdown.Menu>
                    <FormControlLabel
                      control={
                        <Checkbox
                          size="small"
                          name="incomeRange1"
                          onChange={(e) => {
                            setrankRange1(e.target.checked ? "1" : "");
                          }}
                        />
                      }
                      label="0-30"
                    />
                    <FormControlLabel
                      control={
                        <Checkbox
                          size="small"
                          name="incomeRange2"
                          onChange={(e) => {
                            setrankRange2(e.target.checked ? "2" : "");
                          }}
                        />
                      }
                      label="31-60"
                    />
                    <FormControlLabel
                      control={
                        <Checkbox
                          size="small"
                          name="incomeRange3"
                          onChange={(e) => {
                            setrankRange3(e.target.checked ? "3" : "");
                          }}
                        />
                      }
                      label="61-100"
                    />
                  </Dropdown.Menu>
                </Dropdown>
              </Col>
            </Row>
          </Container>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={handleClose}>
            Close
          </Button>
          <Button
            variant="primary"
            onClick={(e) => {
              window.location.href =
                "/schools/n=" +
                nameRange1 +
                nameRange2 +
                nameRange3 +
                "/s=" +
                stateRange1 +
                stateRange2 +
                stateRange3 +
                "/d=" +
                districtRange1 +
                districtRange2 +
                districtRange3 +
                "/l=" +
                levelRange1 +
                levelRange2 +
                levelRange3 +
                "/r=" +
                rankRange1 +
                rankRange2 +
                rankRange3;
            }}
          >
            Apply
          </Button>
        </Modal.Footer>
      </Modal>
      <br></br>
    </Container>
  );
}

/*********************************************************************************/
/******************************** TABLE STYLING **********************************/
/*********************************************************************************/

const StyledTableCell = withStyles((theme) => ({
  head: {
    backgroundColor: theme.palette.common.black,
    color: theme.palette.common.white,
  },
  body: {
    fontSize: 14,
  },
}))(TableCell);

const StyledTableRow = withStyles((theme) => ({
  root: {
    "&:nth-of-type(odd)": {
      backgroundColor: theme.palette.action.hover,
    },
  },
}))(TableRow);

/*********************************************************************************/
/******************************** PAGING CONTROLS ********************************/
/*********************************************************************************/

const useStyles1 = makeStyles((theme) => ({
  root: {
    flexShrink: 0,
    marginLeft: theme.spacing(2.5),
  },
}));

function TablePaginationActions(props) {
  const classes = useStyles1();
  const theme = useTheme();
  const { count, page, rowsPerPage, onChangePage } = props;

  const handleFirstPageButtonClick = (event) => {
    onChangePage(event, 0);
  };

  const handleBackButtonClick = (event) => {
    onChangePage(event, page - 1);
  };

  const handleNextButtonClick = (event) => {
    onChangePage(event, page + 1);
  };

  const handleLastPageButtonClick = (event) => {
    onChangePage(event, Math.max(0, Math.ceil(count / rowsPerPage) - 1));
  };

  return (
    <div className={classes.root}>
      <IconButton
        onClick={handleFirstPageButtonClick}
        disabled={page === 0}
        aria-label="first page"
      >
        {theme.direction === "rtl" ? <LastPageIcon /> : <FirstPageIcon />}
      </IconButton>
      <IconButton
        onClick={handleBackButtonClick}
        disabled={page === 0}
        aria-label="previous page"
      >
        {theme.direction === "rtl" ? (
          <KeyboardArrowRight />
        ) : (
          <KeyboardArrowLeft />
        )}
      </IconButton>
      <IconButton
        onClick={handleNextButtonClick}
        disabled={page >= Math.ceil(count / rowsPerPage) - 1}
        aria-label="next page"
      >
        {theme.direction === "rtl" ? (
          <KeyboardArrowLeft />
        ) : (
          <KeyboardArrowRight />
        )}
      </IconButton>
      <IconButton
        onClick={handleLastPageButtonClick}
        disabled={page >= Math.ceil(count / rowsPerPage) - 1}
        aria-label="last page"
      >
        {theme.direction === "rtl" ? <FirstPageIcon /> : <LastPageIcon />}
      </IconButton>
    </div>
  );
}

TablePaginationActions.propTypes = {
  count: PropTypes.number.isRequired,
  onChangePage: PropTypes.func.isRequired,
  page: PropTypes.number.isRequired,
  rowsPerPage: PropTypes.number.isRequired,
};

/*********************************************************************************/
/******************************** SORTING CONTROLS *******************************/
/*********************************************************************************/

function getComparator(order, orderBy) {
  return order === "desc"
    ? (a, b) => descendingComparator(a, b, orderBy)
    : (a, b) => -descendingComparator(a, b, orderBy);
}

function stableSort(array, comparator) {
  const stabilizedThis = array.map((el, index) => [el, index]);
  stabilizedThis.sort((a, b) => {
    const order = comparator(a[0], b[0]);
    if (order !== 0) return order;
    return a[1] - b[1];
  });
  return stabilizedThis.map((el) => el[0]);
}

EnhancedTableHead.propTypes = {
  classes: PropTypes.object.isRequired,
  onRequestSort: PropTypes.func.isRequired,
  order: PropTypes.oneOf(["asc", "desc"]).isRequired,
  orderBy: PropTypes.string.isRequired,
  rowCount: PropTypes.number.isRequired,
};

function descendingComparator(a, b, orderBy) {
  if (b[orderBy] < a[orderBy]) {
    return -1;
  }
  if (b[orderBy] > a[orderBy]) {
    return 1;
  }
  return 0;
}

// Table Header
const headCells = [
  {
    id: "schoolName",
    numeric: false,
    disablePadding: false,
    label: "School Name",
  },
  { id: "state", numeric: false, disablePadding: false, label: "State" },
  {
    id: "district",
    numeric: false,
    disablePadding: false,
    label: "School District",
  },
  {
    id: "schoolLevel",
    numeric: true,
    disablePadding: false,
    label: "School Level",
  },
  {
    id: "rankPercentile",
    numeric: true,
    disablePadding: false,
    label: "Rank Percentile",
  },
];

function EnhancedTableHead(props) {
  const { classes, order, orderBy, onRequestSort } = props;
  const createSortHandler = (property) => (event) => {
    onRequestSort(event, property);
  };

  return (
    <TableHead>
      <TableRow>
        {headCells.map((headCell) => (
          <TableCell
            key={headCell.id}
            align={headCell.numeric ? "left" : "left"}
            padding={headCell.disablePadding ? "none" : "default"}
            sortDirection={orderBy === headCell.id ? order : false}
          >
            <TableSortLabel
              active={orderBy === headCell.id}
              direction={orderBy === headCell.id ? order : "asc"}
              onClick={createSortHandler(headCell.id)}
            >
              {headCell.label}
              {orderBy === headCell.id ? (
                <span className={classes.visuallyHidden}>
                  {order === "desc" ? "sorted descending" : "sorted ascending"}
                </span>
              ) : null}
            </TableSortLabel>
          </TableCell>
        ))}
      </TableRow>
    </TableHead>
  );
}

EnhancedTableHead.propTypes = {
  classes: PropTypes.object.isRequired,
  onRequestSort: PropTypes.func.isRequired,
  order: PropTypes.oneOf(["asc", "desc"]).isRequired,
  orderBy: PropTypes.string.isRequired,
  rowCount: PropTypes.number.isRequired,
};

const useStyles = makeStyles((theme) => ({
  root: {
    width: "100%",
  },
  paper: {
    width: "100%",
    marginBottom: theme.spacing(2),
  },
  table: {
    minWidth: 750,
  },
  visuallyHidden: {
    border: 0,
    clip: "rect(0 0 0 0)",
    height: 1,
    margin: -1,
    overflow: "hidden",
    padding: 0,
    position: "absolute",
    top: 20,
    width: 1,
  },
}));

/*********************************************************************************/
/******************************** TABLE OUTPUT FN ********************************/
/*********************************************************************************/
const Posts = ({ posts, loading, totalPosts }) => {
  const input = React.useRef();
  const classes = useStyles();

  const [order, setOrder] = React.useState("asc");
  const [orderBy, setOrderBy] = React.useState("schoolName");

  // const classes = useStyles2();
  const [page, setPage] = React.useState(0);
  const [rowsPerPage, setRowsPerPage] = React.useState(6);

  const emptyRows =
    rowsPerPage - Math.min(rowsPerPage, totalPosts - page * rowsPerPage);

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };

  const handleRequestSort = (event, property) => {
    const isAsc = orderBy === property && order === "asc";
    setOrder(isAsc ? "desc" : "asc");
    setOrderBy(property);
  };

  if (loading) {
    return (
      <Typography
        component="h2"
        variant="h2"
        align="center"
        color="textPrimary"
        gutterBottom
      >
        Loading...
      </Typography>
    );
  }

  // for each post ouput a table row
  return (
    <React.Fragment>
      <Typography
        component="h5"
        variant="h5"
        align="center"
        color="textPrimary"
        gutterBottom
      >
        Schools
      </Typography>
      <Form
        className="justify-content-center"
        inline
        onSubmit={(e) => {
          e.preventDefault();
        }}
      >
        <FormControl
          type="text"
          placeholder="Search"
          className="mr-sm-2"
          ref={input}
          onKeyPress={(event) => {
            if (event.key === "Enter") {
              window.location.href =
                "/search/q=" + input.current.value + "/filter=schools";
            }
          }}
        />
        {/* <Button variant="outline-success" onClick={() => this.search()}>Search</Button> */}
      </Form>
      <FilterMenu />
      <TableContainer component={Paper}>
        <Table className={classes.table} aria-label="customized table">
          <EnhancedTableHead
            classes={classes}
            order={order}
            orderBy={orderBy}
            onRequestSort={handleRequestSort}
            rowCount={totalPosts}
          />
          <TableBody>
            {stableSort(posts, getComparator(order, orderBy))
              .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
              .map((post) => (
                <StyledTableRow key={post.id}>
                  <StyledTableCell
                    align="left"
                    scope="row"
                    component="a"
                    href={"/schools/" + post.id}
                  >
                    {" "}
                    {post.schoolName}{" "}
                  </StyledTableCell>
                  <StyledTableCell align="left">{post.state}</StyledTableCell>
                  <StyledTableCell align="left">
                    {post.district}
                  </StyledTableCell>
                  <StyledTableCell align="left">
                    {post.schoolLevel}
                  </StyledTableCell>
                  <StyledTableCell align="left">
                    {post.rankPercentile}
                  </StyledTableCell>
                </StyledTableRow>
              ))}
            {emptyRows > 0 && (
              <TableRow style={{ height: 53 * emptyRows }}>
                <TableCell colSpan={6} />
              </TableRow>
            )}
          </TableBody>
          <TableFooter>
            <TableRow>
              <TablePagination
                rowsPerPageOptions={[6, 9, 12, { label: "All", value: -1 }]}
                colSpan={3}
                count={totalPosts}
                rowsPerPage={rowsPerPage}
                page={page}
                SelectProps={{
                  inputProps: { "aria-label": "rows per page" },
                  native: true,
                }}
                onChangePage={handleChangePage}
                onChangeRowsPerPage={handleChangeRowsPerPage}
                ActionsComponent={TablePaginationActions}
              ></TablePagination>
            </TableRow>
          </TableFooter>
        </Table>
      </TableContainer>
    </React.Fragment>
  );
};

export default Posts;
