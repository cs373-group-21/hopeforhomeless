import React, { useEffect, useState } from "react";
import { Doughnut } from "react-chartjs-2";
import axios from "axios";

import "../../style.css";
import "../../App.css";

function School_Vis() {
  const [isLoaded, setIsLoaded] = useState(false);
  const [items, setItems] = useState([]);

  useEffect(() => {
    const fetchPosts = async () => {
      setIsLoaded(false);
      let url = "https://api.hopeforhomeless.me/schools";
      const res = await axios.get(url);
      const info = res["data"];
      const ethnicities = new Array(7).fill(0);
      for (var index in info) {
        var school = info[index];
        var num = school.numStudents;
        ethnicities[0] += (school.pctAfricanAmerican / 100) * num;
        ethnicities[1] += (school.pctAsian / 100) * num;
        ethnicities[2] += (school.pctHispanic / 100) * num;
        ethnicities[3] += (school.pctIndian / 100) * num;
        ethnicities[4] += (school.pctMixed / 100) * num;
        ethnicities[5] += (school.pctPacificIslander / 100) * num;
        ethnicities[6] += (school.pctWhite / 100) * num;
      }
      for (var i = 0; i < 7; i++) ethnicities[i] = Math.round(ethnicities[i]);
      setItems(ethnicities);
      setIsLoaded(true);
    };

    fetchPosts();
  }, []);
  if (!isLoaded) {
    return <div>Loading..</div>;
  } else {
    const data = {
      labels: [
        "African American",
        "Asian",
        "Hispanic",
        "Native American",
        "Mixed",
        "Pacific Islander",
        "White",
      ],
      datasets: [
        {
          data: items,
          backgroundColor: [
            "#003F5C",
            " #FFC154",
            "#CC1FC7",
            "#EC6B56",
            "#47B39C",
            "#9552EA",
            "#7CDDDD",
          ],
        },
      ],
    };

    return (
      <div>
        <div className="vis-container">
          <h1>Racial/Ethnic Enrollment in Public Schools</h1>
          <p>
            This chart shows the distribution of students in public schools by
            racial and ethnic background, accumulated throughout all major
            cities and urban areas in the United States.
          </p>
          <Doughnut data={data} />
        </div>
      </div>
    );
  }
}
export default School_Vis;
