import React, { useEffect, useState } from "react";
import { Pie } from "react-chartjs-2";
import axios from "axios";

import "../../style.css";
import "../../App.css";

function P2Vis() {
  const [isLoaded, setIsLoaded] = useState(false);
  const [names, setNames] = useState([]);
  const [flights, setFlights] = useState([]);

  useEffect(() => {
    const fetchPosts = async () => {
      setIsLoaded(false);
      // Get all countries ordered by population
      let url =
        "https://travelscares.me/api/routes?&orderBy=arrival_country&ordering=desc&page=1&pageSize=8537";
      const res = await axios.get(url);
      const info = res["data"];
      const countries = info["results"];

      var resultNames = new Array(7).fill("");
      var resultFlights = new Array(7).fill(0);

      let currflight = 0;
      var currTempName;
      var lastTempName;
      var flightNames = new Array(7).fill("");
      var flights = new Array(7).fill(0);
      for (var i = 0; i < countries.length; i++) {
        var country = countries[i];
        currTempName = country.arrival_country;
        if (i === 0) {
          lastTempName = currTempName;
          currflight++;
        } else {
          if (currTempName === lastTempName) {
            currflight++;
          } else {
            let n = 0;
            while (n < 7 && flights[n] !== 0) {
              if (currflight >= flights[n]) {
                flightNames.splice(n, 0, lastTempName);
                flights.splice(n, 0, currflight);
                break;
              } else {
                n++;
              }
            }
            if (n < 7 && flights[n] === 0) {
              flightNames.splice(n, 0, lastTempName);
              flights.splice(n, 0, currflight);
            }
            lastTempName = currTempName;
            currflight = 1;
          }
        }
      }

      for (i = 0; i < 7; i++) {
        resultNames[i] = flightNames[i];
        resultFlights[i] = flights[i];
      }

      setNames(resultNames);
      setFlights(resultFlights);
      setIsLoaded(true);
    };

    fetchPosts();
  }, []);

  if (!isLoaded) {
    return <div>Loading..</div>;
  } else {
    const data = {
      labels: names,
      datasets: [
        {
          data: flights,
          backgroundColor: [
            "#003F5C",
            " #FFC154",
            "#CC1FC7",
            "#EC6B56",
            "#47B39C",
            "#9552EA",
            "#7CDDDD",
          ],
        },
      ],
    };

    return (
      <div>
        <div className="vis-container">
          <h1>7 Most Traveled To Countries</h1>
          <p>
            This chart displays the top 7 countries most commonly listed as the
            destination for airplane routes.
          </p>
          <Pie data={data} />
        </div>
      </div>
    );
  }
}

export default P2Vis;
