import React from "react";

import "./CityVis";
import "./OrgVis";
import "./SchoolVis";
import "./P1Vis";
import "./P2Vis";
import "./P3Vis";
import "../../style.css";
import "../../App.css";

import Container from "@material-ui/core/Container";
import { Dropdown } from "react-bootstrap";
import P1Vis from "./P1Vis";
import P2Vis from "./P2Vis";
import P3Vis from "./P3Vis";

const FilterMenu = () => (
  <Container maxWidth="sm">
    <Dropdown>
      <Dropdown.Toggle variant="primary" id="dropdown-basic">
        Select
      </Dropdown.Toggle>

      <Dropdown.Menu>
        <Dropdown.Item href={"/visualizations/provider/countries"}>
          Countries
        </Dropdown.Item>
        <Dropdown.Item href={"/visualizations/provider/airplanes"}>
          Airplane Routes
        </Dropdown.Item>
        <Dropdown.Item href={"/visualizations/provider/articles"}>
          Articles
        </Dropdown.Item>
      </Dropdown.Menu>
    </Dropdown>
  </Container>
);

function PVisualization(props) {
  const vis = props.match.params.vis;
  switch (vis) {
    case "countries":
      return (
        <div className="content">
          <FilterMenu />
          <br></br>
          <P1Vis />
        </div>
      );
    case "airplanes":
      return (
        <div className="content">
          <FilterMenu />
          <br></br>
          <P2Vis />
        </div>
      );
    case "articles":
      return (
        <div className="content">
          <FilterMenu />
          <br></br>
          <P3Vis />
        </div>
      );
    default:
      return (
        <div className="content">
          <FilterMenu />
          <h1>Invalid Visualization</h1>
        </div>
      );
  }
}
export default PVisualization;
