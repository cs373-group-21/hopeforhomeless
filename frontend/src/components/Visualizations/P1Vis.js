import React, { useEffect, useState } from "react";
import axios from "axios";

import { Bar } from "react-chartjs-2";

import "../../style.css";
import "../../App.css";

function P1Vis() {
  const [isLoaded, setIsLoaded] = useState(false);
  const [names, setNames] = useState([]);
  const [pops, setPops] = useState([]);

  useEffect(() => {
    const fetchPosts = async () => {
      setIsLoaded(false);
      // Get all countries ordered by population
      let url =
        "https://travelscares.me/api/countries?&orderBy=population&ordering=desc&page=1&pageSize=250&populationMin=25300000";
      const res = await axios.get(url);
      const info = res["data"];
      const countries = info["results"];

      const countryNames = new Array(50).fill("");
      const population = new Array(50).fill(0);
      for (var i = 0; i < countries.length; i++) {
        var country = countries[i];
        countryNames[i] = country.name;
        population[i] = country.population;
      }

      // console.log(countryNames);
      // console.log(population);

      setNames(countryNames);
      setPops(population);
      setIsLoaded(true);
    };

    fetchPosts();
  }, []);

  if (!isLoaded) {
    return <div>Loading..</div>;
  } else {
    const data = {
      labels: names,
      display: true,
      datasets: [
        {
          label: "Population",
          data: pops,
          backgroundColor: [
            "#003F5C",
            " #FFC154",
            "#CC1FC7",
            "#EC6B56",
            "#47B39C",
            "#9552EA",
            "#7CDDDD",
          ],
        },
      ],
    };

    const options = {
      scales: {
        y: {
          title: {
            display: true,
            text: "Population",
          },
          ticks: {
            beginAtZero: true,
          },
        },
        x: {
          title: {
            display: true,
            text: "Country",
          },
        },
      },
    };

    return (
      <div>
        <div className="vis-container">
          <h1>50 Most Populous Countries</h1>
          <p>
            This chart shows the top 50 countries in the world, ranked by
            population.
          </p>
          <Bar data={data} options={options} height={5000} width={7000} />
        </div>
      </div>
    );
  }
}

export default P1Vis;
