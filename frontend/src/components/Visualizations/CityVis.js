import React, { useEffect, useState } from "react";
import { Bubble } from "react-chartjs-2";
import axios from "axios";

import "../../style.css";
import "../../App.css";

function CityVis() {
  const [isLoaded, setIsLoaded] = useState(false);
  const [items, setItems] = useState([]);
  useEffect(() => {
    const fetchPosts = async () => {
      setIsLoaded(false);
      let url = "https://api.hopeforhomeless.me/cities";
      const res = await axios.get(url);
      const info = res["data"];
      const cityData = new Array(112).fill(0);
      for (var index in info) {
        var city = info[index];
        cityData[index] = {
          label: city.city,
          data: [
            {
              x: city.medianIncome,
              y: (city.povertyPopulation / city.totalPopulation) * 100,
              r: city.medianRent / 200,
            },
          ],
          backgroundColor:
            "#" + Math.floor(Math.random() * 16777215).toString(16), // generate a random color
        };
      }
      setItems(cityData);
      setIsLoaded(true);
    };

    fetchPosts();
  }, []);
  if (!isLoaded) {
    return <div>Loading..</div>;
  } else {
    const data = {
      datasets: items,
    };

    const chartOptions = {
      scales: {
        x: {
          title: {
            display: true,
            text: "Median Income (USD)",
          },
        },
        y: {
          title: {
            display: true,
            text: "% Population in Poverty",
          },
        },
      },
      plugins: {
        legend: {
          display: false,
        },
      },
    };

    return (
      <div>
        <div className="vis-container">
          <h1>Median Rent vs. Median Income vs. Percent in Poverty by City</h1>
          <p>
            This figure shows the relationship between the median income,
            impoverished population, and the median rent for major cities and
            urban areas across the United States.The x-axis shows the median
            income for a given city. The y-axis shows the percentage of the
            population classified as impoverished by the US Census. The size of
            each bubble corresponds to the median rent in a given city.
          </p>
          <Bubble data={data} options={chartOptions} />
        </div>
      </div>
    );
  }
}
export default CityVis;
