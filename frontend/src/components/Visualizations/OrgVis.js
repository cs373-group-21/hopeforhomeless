//Source: https://github.com/reactchartjs/react-chartjs-2/blob/67cd5622d702230f1aa5e06d994134fdd451bd7b/example/src/charts/HorizontalBar.js
import { Bar } from "react-chartjs-2";
import axios from "axios";
import React, { useEffect, useState } from "react";

import "../../style.css";
import "../../App.css";

function Org_Vis() {
  const [isLoaded, setIsLoaded] = useState(false);
  const [items, setItems] = useState([]);
  const options = {
    indexAxis: "y",
    plugins: {
      legend: {
        display: false,
      },
    },
    scales: {
      x: {
        title: {
          display: true,
          text: "Number of Organizations",
        },
      },
      y: {
        title: {
          display: true,
          text: "Cause",
        },
      },
    },
  };

  useEffect(() => {
    const fetchPosts = async () => {
      setIsLoaded(false);
      let url = "https://api.hopeforhomeless.me/organizations";
      const res = await axios.get(url);
      const info = res["data"];
      const causes = new Array(8).fill(0);
      for (var index in info) {
        var org = info[index];
        //var num = school.numStudents;
        if (org.cause === "Rescue Missions") {
          causes[0] += 1;
        }
        if (org.cause === "Homeless Services") {
          causes[1] += 1;
        }
        if (org.cause === "Social Services") {
          causes[2] += 1;
        }
        if (org.cause === "Children's and Family Services") {
          causes[3] += 1;
        }
        if (org.cause === "Youth Development, Shelter, and Crisis Services") {
          causes[4] += 1;
        }
        if (org.cause === "Food Banks, Food Pantries, and Food Distribution") {
          causes[5] += 1;
        }
        if (org.cause === "Patient and Family Support") {
          causes[6] += 1;
        }
        if (org.cause === "Multipurpose Human Service Organizations") {
          causes[7] += 1;
        }
      }
      setItems(causes);
      setIsLoaded(true);
    };
    fetchPosts();
  }, []);
  if (!isLoaded) {
    return <div>Loading..</div>;
  } else {
    const data = {
      labels: [
        "Rescue Missions",
        "Homeless Services",
        "Social Services",
        "Children's and Family Services",
        "Youth Development, Shelter, and Crisis Services",
        "Food Banks, Food Pantries, and Food Distribution",
        "Patient and Family Support",
        "Multipurpose Human Service Organizations",
      ],
      datasets: [
        {
          label: "Number of Organizations",
          data: items,
          backgroundColor: [
            "#003F5C",
            " #FFC154",
            "#CC1FC7",
            "#EC6B56",
            "#47B39C",
            "#9552EA",
            "#7CDDDD",
            "#003F5C",
          ],
        },
      ],
    };

    return (
      <div className="vis-container">
        <h1>Organizations by Cause</h1>
        <p>
          This graphic shows the number of charitable organizations for each
          cause present in major cities across the United States. The x axis
          shows the number of organizations and the y axis shows the cause.
        </p>
        <Bar data={data} options={options} />
      </div>
    );
  }
}
export default Org_Vis;
