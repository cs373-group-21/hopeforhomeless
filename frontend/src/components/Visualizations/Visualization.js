import React from "react";

import "./CityVis";
import "./OrgVis";
import "./SchoolVis";
import "./P1Vis";
import "./P2Vis";
import "./P3Vis";
import "../../style.css";
import "../../App.css";

import Container from "@material-ui/core/Container";
import { Dropdown } from "react-bootstrap";
import CityVis from "./CityVis";
import OrgVis from "./OrgVis";
import SchoolVis from "./SchoolVis";

const FilterMenu = () => (
  <Container maxWidth="sm">
    <Dropdown>
      <Dropdown.Toggle variant="primary" id="dropdown-basic">
        Select
      </Dropdown.Toggle>

      <Dropdown.Menu>
        <Dropdown.Item href={"/visualizations/hfh/cities"}>
          Cities
        </Dropdown.Item>
        <Dropdown.Item href={"/visualizations/hfh/organizations"}>
          Organizations
        </Dropdown.Item>
        <Dropdown.Item href={"/visualizations/hfh/schools"}>
          Schools
        </Dropdown.Item>
      </Dropdown.Menu>
    </Dropdown>
  </Container>
);

function Visualization(props) {
  const vis = props.match.params.v;
  console.log(vis);
  switch (vis) {
    case "cities":
      return (
        <div className="content">
          <FilterMenu />
          <br></br>
          <CityVis />
        </div>
      );
    case "organizations":
      return (
        <div className="content">
          <FilterMenu />
          <br></br>
          <OrgVis />
        </div>
      );
    case "schools":
      return (
        <div className="content">
          <FilterMenu />
          <br></br>
          <SchoolVis />
        </div>
      );
    default:
      return (
        <div className="content">
          <FilterMenu />
          <h1>Invalid Visualization</h1>
        </div>
      );
  }
}
export default Visualization;
