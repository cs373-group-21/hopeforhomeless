import React, { useEffect, useState } from "react";
import { PolarArea } from "react-chartjs-2";
import axios from "axios";

import "../../style.css";
import "../../App.css";

function P3Vis() {
  const [isLoaded, setIsLoaded] = useState(false);
  const [items, setItems] = useState([]);
  useEffect(() => {
    const fetchPosts = async () => {
      setIsLoaded(false);
      const articleData = new Array(3).fill(0);
      let url = "https://travelscares.me/api/articles?page=1&pageSize=8000";
      const res = await axios.get(url);
      const info = res["data"];
      const data = info["results"];
      for (var i = 0; i < data.length; i++) {
        var category = data[i];
        category = category.category;
        if (category === "Health") {
          articleData[0]++;
        } else if (category === "Technology") {
          articleData[1]++;
        } else {
          articleData[2]++;
        }
      }
      setItems(articleData);
      setIsLoaded(true);
    };

    fetchPosts();
  }, []);
  if (!isLoaded) {
    return <div>Loading..</div>;
  } else {
    const data = {
      labels: ["Health", "Technology", "Science"],
      display: true,
      datasets: [
        {
          data: items,
          backgroundColor: ["#ff6363", "#8affb5", "#83cafc"],
        },
      ],
    };

    const options = {
      plugins: {
        legend: {
          display: false,
        },
      },
    };

    return (
      <div>
        <h1>Breakdown of Articles by Category</h1>
        <div className="vis-container">
          <p>
            This graphic shows the distribution of the categories of news
            articles written across the world.
          </p>
          <PolarArea data={data} options={options} />
        </div>
      </div>
    );
  }
}
export default P3Vis;
