// Source: ui material
import React, { useEffect, useState } from "react";
import Typography from "@material-ui/core/Typography";
import PostsSchool from "./SchoolModelPage/Posts_School";

import { makeStyles } from "@material-ui/core/styles";
import Paper from "@material-ui/core/Paper";
import Grid from "@material-ui/core/Grid";

import "../style.css";
import "../App.css";

function Charitable_organizations_instance(props) {
  //https://reactjs.org/docs/faq-ajax.html

  const [items, setItems] = useState([]);
  const [items2, setItems2] = useState([]);

  let id = props.match.params.id;

  useEffect(() => {
    fetch("https://api.hopeforhomeless.me/organizations/" + id)
      .then((res) => res.json())
      .then((result) => {
        setItems(result);
        fetch("https://api.hopeforhomeless.me/schools?cityId=" + result.cityId)
          .then((res) => res.json())
          .then((result) => {
            console.log(result);
            setItems2(result);
          });
      });
  });

  const useStyles = makeStyles((theme) => ({
    root: {
      flexGrow: 1,
    },
    paper: {
      padding: theme.spacing(1),
      textAlign: "center",
      color: theme.palette.text.primary,
    },
  }));

  const classes = useStyles();

  return (
    <div className="Charitable_organizations_instance">
      <body>
        <main>
          <div className="content">
            <div className="cityDescription">
              <div className="cityPage">
                <span className="title">
                  <span
                    className="ant-typography"
                    styles="font-size: 24px;"
                  ></span>
                </span>
                <div className="cityDescription">
                  <h2 className="ant-typography" styles="text-align: center;">
                    {items.name}
                  </h2>
                  <img
                    width="600"
                    height="400"
                    src={items.image}
                    alt="City_1_IMG"
                    className="organizationImage"
                  />

                  <article className="cityDescription">
                    <Typography
                      component="h5"
                      variant="h5"
                      align="center"
                      color="textPrimary"
                      gutterBottom
                    >
                      Mission:
                    </Typography>
                    <div>
                      <Typography>
                        <div
                          dangerouslySetInnerHTML={{ __html: items.mission }}
                        ></div>
                      </Typography>
                      <br></br>
                    </div>
                  </article>

                  <article className="cityDescription">
                    <div className="cityDescription">
                      <div className={classes.root}>
                        <Grid container spacing={2}>
                          <Grid item xs={12}>
                            <Paper className={classes.paper}>
                              {items.tagline}
                            </Paper>
                          </Grid>

                          <Grid item xs={6}>
                            <Paper className={classes.paper}>
                              <strong>Category:</strong>
                            </Paper>
                          </Grid>
                          <Grid item xs={6}>
                            <Paper className={classes.paper}>
                              {items.cause}
                            </Paper>
                          </Grid>

                          <Grid item xs={6}>
                            <Paper className={classes.paper}>
                              <strong>Annual Income:</strong>
                            </Paper>
                          </Grid>
                          <Grid item xs={6}>
                            <Paper className={classes.paper}>
                              ${items.income}
                            </Paper>
                          </Grid>

                          <Grid item xs={6}>
                            <Paper className={classes.paper}>
                              <strong>Rating:</strong>
                            </Paper>
                          </Grid>
                          <Grid item xs={6}>
                            <Paper className={classes.paper}>
                              {items.rating}
                            </Paper>
                          </Grid>

                          <Grid item xs={6}>
                            <Paper className={classes.paper}>
                              <strong>IRS Class:</strong>
                            </Paper>
                          </Grid>
                          <Grid item xs={6}>
                            <Paper className={classes.paper}>
                              {items.irs_class}
                            </Paper>
                          </Grid>
                        </Grid>
                      </div>
                    </div>
                  </article>

                  <div class="ant-typography">Map of {items.name}</div>

                  <iframe
                    title="Organization map"
                    src={items.map}
                    width="600"
                    height="450"
                    styles="border:0;"
                    allowfullscreen=""
                    loading="lazy"
                  ></iframe>
                  <Grid container spacing={2}>
                    <Grid item xs={6}>
                      <Paper className={classes.paper}>
                        <strong>Location:</strong>
                      </Paper>
                    </Grid>
                    <Grid item xs={6}>
                      <Paper className={classes.paper}>{items.city}</Paper>
                    </Grid>

                    <Grid item xs={6}>
                      <Paper className={classes.paper}>
                        <strong>Address:</strong>
                      </Paper>
                    </Grid>
                    <Grid item xs={6}>
                      <Paper className={classes.paper}>{items.address}</Paper>
                    </Grid>

                    <Grid item xs={6}>
                      <Paper className={classes.paper}>
                        <strong>Zip Code:</strong>
                      </Paper>
                    </Grid>
                    <Grid item xs={6}>
                      <Paper className={classes.paper}>{items.zip}</Paper>
                    </Grid>
                  </Grid>

                  <article className="cityOtherDetails">
                    {/* <Typography component = "h5" variant="h5" align="center" color="textPrimary" gutterBottom>Other Information</Typography> */}
                    <section>
                      <Typography
                        component="h5"
                        variant="h5"
                        align="center"
                        color="textPrimary"
                        gutterBottom
                      >
                        Learn more about this city:
                      </Typography>
                      <div>
                        <a href={"/locations/" + items.cityId}>{items.city}</a>
                      </div>
                    </section>

                    <section>
                      {/* SCHOOLS TABLE */}
                      <React.Fragment>
                        <Typography
                          component="h5"
                          variant="h5"
                          align="left"
                          color="textPrimary"
                          gutterBottom
                        >
                          {"\n"}
                        </Typography>
                        <PostsSchool
                          posts={items2}
                          loading={null}
                          totalPosts={items2.length}
                        />
                      </React.Fragment>
                    </section>
                  </article>
                </div>
              </div>
            </div>
          </div>
        </main>
        <footer></footer>
        <script
          src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/js/bootstrap.bundle.min.js"
          integrity="sha384-b5kHyXgcpbZJO/tY9Ul7kGkf1S0CWuKcCD38l8YkeH8z8QjE0GmW1gYU5S9FOnJ0"
          crossorigin="anonymous"
        ></script>
      </body>
    </div>
  );
}

export default Charitable_organizations_instance;
