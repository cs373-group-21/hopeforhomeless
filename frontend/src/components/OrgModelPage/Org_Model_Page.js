// Source:

// https://github.com/bradtraversy/simple_react_pagination
// https://www.youtube.com/watch?v=IYCa1F-OWmk

import React, { useState, useEffect } from "react";
import Posts from "./Posts_Org";
import axios from "axios";
import Container from "@material-ui/core/Container";

// https://reactjs.org/docs/faq-ajax.html
const APICallTest = (props) => {
  const [posts, setPosts] = useState([]);
  const [loading, setLoading] = useState(false);

  //const [currentPage, setCurrentPage] = useState(1);
  //const [postsPerPage] = useState(10);

  const name =
    props.match.params.name !== undefined ? props.match.params.name : "";
  const city =
    props.match.params.city !== undefined ? props.match.params.city : "";
  const category =
    props.match.params.category !== undefined
      ? props.match.params.category
      : "";
  const income =
    props.match.params.income !== undefined ? props.match.params.income : "";
  const zip =
    props.match.params.zip !== undefined ? props.match.params.zip : "";

  useEffect(() => {
    const fetchPosts = async () => {
      setLoading(true);
      let url =
        "https://api.hopeforhomeless.me/organizations?fname=" +
        name +
        "&fcity=" +
        city +
        "&fcause=" +
        category +
        "&fincome=" +
        income +
        "&fzip=" +
        zip;
      const res = await axios.get(url);
      const info = res["data"];
      setPosts(info);
      setLoading(false);
    };

    fetchPosts();
  }, [category, city, income, name, zip]);

  return (
    <React.Fragment>
      <Container>
        <div className="content">
          {/* <Typography component = "h2" variant="h2" align="center" color="textPrimary" gutterBottom>Organizations</Typography> */}
          <Posts posts={posts} loading={loading} totalPosts={posts.length} />
        </div>
      </Container>
    </React.Fragment>
  );
};

export default APICallTest;
