// Sources:

// https://github.com/bradtraversy/simple_react_pagination
// https://www.youtube.com/watch?v=IYCa1F-OWmk
// https://material-ui.com/components/tables/
// using Burnin Up project and react bootstrap as references

import React from "react";
import { withStyles, makeStyles, useTheme } from "@material-ui/core/styles";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Paper from "@material-ui/core/Paper";
import Typography from "@material-ui/core/Typography";
import IconButton from "@material-ui/core/IconButton";
import FirstPageIcon from "@material-ui/icons/FirstPage";
import KeyboardArrowLeft from "@material-ui/icons/KeyboardArrowLeft";
import KeyboardArrowRight from "@material-ui/icons/KeyboardArrowRight";
import LastPageIcon from "@material-ui/icons/LastPage";
import TableFooter from "@material-ui/core/TableFooter";
import PropTypes from "prop-types";
import TablePagination from "@material-ui/core/TablePagination";
import TableSortLabel from "@material-ui/core/TableSortLabel";
import { Col, Form, FormControl } from "react-bootstrap";
import Checkbox from "@material-ui/core/Checkbox";
import Container from "@material-ui/core/Container";
import { Row, Dropdown, Modal } from "react-bootstrap";
import Button from "@material-ui/core/Button";
import FormControlLabel from "@material-ui/core/FormControlLabel";

/*********************************************************************************/
/******************************** FILTER MENU ************************************/
/*********************************************************************************/

function FilterMenu() {
  // const classes = useStyles();
  const [nameRange1, setNameRange1] = React.useState("");
  const [nameRange2, setNameRange2] = React.useState("");
  const [nameRange3, setNameRange3] = React.useState("");

  const [cityRange1, setCityRange1] = React.useState("");
  const [cityRange2, setCityRange2] = React.useState("");
  const [cityRange3, setCityRange3] = React.useState("");

  const [incomeRange1, setincomeRange1] = React.useState("");
  const [incomeRange2, setincomeRange2] = React.useState("");
  const [incomeRange3, setincomeRange3] = React.useState("");

  const [categoryRange1, setcategoryRange1] = React.useState("");
  const [categoryRange2, setcategoryRange2] = React.useState("");
  const [categoryRange3, setcategoryRange3] = React.useState("");

  const [zipRange1, setzipRange1] = React.useState("");
  const [zipRange2, setzipRange2] = React.useState("");
  const [zipRange3, setzipRange3] = React.useState("");

  const [show, setShow] = React.useState(false);

  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

  const colStyle = {
    padding: "0.4rem",
  };

  return (
    <Container maxWidth="lg">
      <Button style={{ float: "left" }} onClick={handleShow}>
        Filter
      </Button>
      <Modal show={show} onHide={handleClose} backdrop="static">
        <Modal.Header closeButton>
          <Modal.Title>Filters</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Container fluid>
            <Row className="text-center">
              <Col style={colStyle}>
                <Dropdown>
                  <Dropdown.Toggle size="sm" id="dropdown-basic">
                    Name
                  </Dropdown.Toggle>
                  <Dropdown.Menu>
                    <FormControlLabel
                      control={
                        <Checkbox
                          size="small"
                          name="nameRange1"
                          onChange={(e) => {
                            setNameRange1(e.target.checked ? "a" : "");
                          }}
                        />
                      }
                      label="A-I"
                    />
                    <FormControlLabel
                      control={
                        <Checkbox
                          size="small"
                          name="nameRange2"
                          onChange={(e) => {
                            setNameRange2(e.target.checked ? "j" : "");
                          }}
                        />
                      }
                      label="J-Q"
                    />
                    <FormControlLabel
                      control={
                        <Checkbox
                          size="small"
                          name="nameRange3"
                          onChange={(e) => {
                            setNameRange3(e.target.checked ? "r" : "");
                          }}
                        />
                      }
                      label="R-Z"
                    />
                  </Dropdown.Menu>
                </Dropdown>
              </Col>
              <Col style={colStyle}>
                <Dropdown>
                  <Dropdown.Toggle size="sm" id="dropdown-basic">
                    City
                  </Dropdown.Toggle>
                  <Dropdown.Menu>
                    <FormControlLabel
                      control={
                        <Checkbox
                          size="small"
                          name="cityRange1"
                          onChange={(e) => {
                            setCityRange1(e.target.checked ? "a" : "");
                          }}
                        />
                      }
                      label="A-I"
                    />
                    <FormControlLabel
                      control={
                        <Checkbox
                          size="small"
                          name="cityRange2"
                          onChange={(e) => {
                            setCityRange2(e.target.checked ? "j" : "");
                          }}
                        />
                      }
                      label="J-Q"
                    />
                    <FormControlLabel
                      control={
                        <Checkbox
                          size="small"
                          name="cityRange3"
                          onChange={(e) => {
                            setCityRange3(e.target.checked ? "r" : "");
                          }}
                        />
                      }
                      label="R-Z"
                    />
                  </Dropdown.Menu>
                </Dropdown>
              </Col>
              <Col style={colStyle}>
                <Dropdown>
                  <Dropdown.Toggle size="sm" id="dropdown-basic">
                    Cause
                  </Dropdown.Toggle>
                  <Dropdown.Menu>
                    <FormControlLabel
                      control={
                        <Checkbox
                          size="small"
                          name="categoryRange1"
                          onChange={(e) => {
                            setcategoryRange1(e.target.checked ? "a" : "");
                          }}
                        />
                      }
                      label="A-I"
                    />
                    <FormControlLabel
                      control={
                        <Checkbox
                          size="small"
                          name="categoryRange2"
                          onChange={(e) => {
                            setcategoryRange2(e.target.checked ? "j" : "");
                          }}
                        />
                      }
                      label="J-Q"
                    />
                    <FormControlLabel
                      control={
                        <Checkbox
                          size="small"
                          name="categoryRange3"
                          onChange={(e) => {
                            setcategoryRange3(e.target.checked ? "r" : "");
                          }}
                        />
                      }
                      label="R-Z"
                    />
                  </Dropdown.Menu>
                </Dropdown>
              </Col>
              <Col style={colStyle}>
                <Dropdown>
                  <Dropdown.Toggle size="sm" id="dropdown-basic">
                    Zip
                  </Dropdown.Toggle>
                  <Dropdown.Menu>
                    <FormControlLabel
                      control={
                        <Checkbox
                          size="small"
                          name="zipRange1"
                          onChange={(e) => {
                            setzipRange1(e.target.checked ? "1" : "");
                          }}
                        />
                      }
                      label="00000-30000"
                    />
                    <FormControlLabel
                      control={
                        <Checkbox
                          size="small"
                          name="zipRange2"
                          onChange={(e) => {
                            setzipRange2(e.target.checked ? "2" : "");
                          }}
                        />
                      }
                      label="30001-60000"
                    />
                    <FormControlLabel
                      control={
                        <Checkbox
                          size="small"
                          name="zipRange3"
                          onChange={(e) => {
                            setzipRange3(e.target.checked ? "3" : "");
                          }}
                        />
                      }
                      label="60001-99999"
                    />
                  </Dropdown.Menu>
                </Dropdown>
              </Col>
              <Col style={colStyle}>
                <Dropdown>
                  <Dropdown.Toggle size="sm" id="dropdown-basic">
                    Income
                  </Dropdown.Toggle>
                  <Dropdown.Menu>
                    <FormControlLabel
                      control={
                        <Checkbox
                          size="small"
                          name="incomeRange1"
                          onChange={(e) => {
                            setincomeRange1(e.target.checked ? "1" : "");
                          }}
                        />
                      }
                      label="0-3,000,000"
                    />
                    <FormControlLabel
                      control={
                        <Checkbox
                          size="small"
                          name="incomeRange2"
                          onChange={(e) => {
                            setincomeRange2(e.target.checked ? "2" : "");
                          }}
                        />
                      }
                      label="3,000,001-10,000,000"
                    />
                    <FormControlLabel
                      control={
                        <Checkbox
                          size="small"
                          name="incomeRange3"
                          onChange={(e) => {
                            setincomeRange3(e.target.checked ? "3" : "");
                          }}
                        />
                      }
                      label="10,000,001+"
                    />
                  </Dropdown.Menu>
                </Dropdown>
              </Col>
            </Row>
          </Container>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={handleClose}>
            Close
          </Button>
          <Button
            variant="primary"
            onClick={(e) => {
              window.location.href =
                "/organizations/n=" +
                nameRange1 +
                nameRange2 +
                nameRange3 +
                "/ct=" +
                cityRange1 +
                cityRange2 +
                cityRange3 +
                "/ca=" +
                categoryRange1 +
                categoryRange2 +
                categoryRange3 +
                "/z=" +
                zipRange1 +
                zipRange2 +
                zipRange3 +
                "/i=" +
                incomeRange1 +
                incomeRange2 +
                incomeRange3;
            }}
          >
            Apply
          </Button>
        </Modal.Footer>
      </Modal>
      <br></br>
    </Container>
  );
}

/*********************************************************************************/
/******************************** TABLE STYLING **********************************/
/*********************************************************************************/

const StyledTableCell = withStyles((theme) => ({
  head: {
    backgroundColor: theme.palette.common.black,
    color: theme.palette.common.white,
  },
  body: {
    fontSize: 14,
  },
}))(TableCell);

const StyledTableRow = withStyles((theme) => ({
  root: {
    "&:nth-of-type(odd)": {
      backgroundColor: theme.palette.action.hover,
    },
  },
}))(TableRow);

/*********************************************************************************/
/******************************** PAGING CONTROLS ********************************/
/*********************************************************************************/

const useStyles1 = makeStyles((theme) => ({
  root: {
    flexShrink: 0,
    marginLeft: theme.spacing(2.5),
  },
}));

function TablePaginationActions(props) {
  const classes = useStyles1();
  const theme = useTheme();
  const { count, page, rowsPerPage, onChangePage } = props;

  const handleFirstPageButtonClick = (event) => {
    onChangePage(event, 0);
  };

  const handleBackButtonClick = (event) => {
    onChangePage(event, page - 1);
  };

  const handleNextButtonClick = (event) => {
    onChangePage(event, page + 1);
  };

  const handleLastPageButtonClick = (event) => {
    onChangePage(event, Math.max(0, Math.ceil(count / rowsPerPage) - 1));
  };

  return (
    <div className={classes.root}>
      <IconButton
        onClick={handleFirstPageButtonClick}
        disabled={page === 0}
        aria-label="first page"
      >
        {theme.direction === "rtl" ? <LastPageIcon /> : <FirstPageIcon />}
      </IconButton>
      <IconButton
        onClick={handleBackButtonClick}
        disabled={page === 0}
        aria-label="previous page"
      >
        {theme.direction === "rtl" ? (
          <KeyboardArrowRight />
        ) : (
          <KeyboardArrowLeft />
        )}
      </IconButton>
      <IconButton
        onClick={handleNextButtonClick}
        disabled={page >= Math.ceil(count / rowsPerPage) - 1}
        aria-label="next page"
      >
        {theme.direction === "rtl" ? (
          <KeyboardArrowLeft />
        ) : (
          <KeyboardArrowRight />
        )}
      </IconButton>
      <IconButton
        onClick={handleLastPageButtonClick}
        disabled={page >= Math.ceil(count / rowsPerPage) - 1}
        aria-label="last page"
      >
        {theme.direction === "rtl" ? <FirstPageIcon /> : <LastPageIcon />}
      </IconButton>
    </div>
  );
}

TablePaginationActions.propTypes = {
  count: PropTypes.number.isRequired,
  onChangePage: PropTypes.func.isRequired,
  page: PropTypes.number.isRequired,
  rowsPerPage: PropTypes.number.isRequired,
};

/*********************************************************************************/
/******************************** FILTER CONTROLS ********************************/
/*********************************************************************************/
// const filters = {
//   name: new Set()
// }

// function filterItems(){

// }

/*********************************************************************************/
/******************************** SORTING CONTROLS *******************************/
/*********************************************************************************/

function getComparator(order, orderBy) {
  return order === "desc"
    ? (a, b) => descendingComparator(a, b, orderBy)
    : (a, b) => -descendingComparator(a, b, orderBy);
}

function stableSort(array, comparator) {
  const stabilizedThis = array.map((el, index) => [el, index]);
  stabilizedThis.sort((a, b) => {
    const order = comparator(a[0], b[0]);
    if (order !== 0) return order;
    return a[1] - b[1];
  });
  return stabilizedThis.map((el) => el[0]);
}
EnhancedTableHead.propTypes = {
  classes: PropTypes.object.isRequired,
  onRequestSort: PropTypes.func.isRequired,
  order: PropTypes.oneOf(["asc", "desc"]).isRequired,
  orderBy: PropTypes.string.isRequired,
  rowCount: PropTypes.number.isRequired,
};

function descendingComparator(a, b, orderBy) {
  if (b[orderBy] < a[orderBy]) {
    return -1;
  }
  if (b[orderBy] > a[orderBy]) {
    return 1;
  }
  return 0;
}

// Table Header
const headCells = [
  {
    id: "name",
    numeric: false,
    disablePadding: false,
    label: "Organization Name",
  },
  { id: "income", numeric: true, disablePadding: false, label: "Income ($)" },
  { id: "cause", numeric: false, disablePadding: false, label: "Cause" },
  { id: "city", numeric: false, disablePadding: false, label: "City" },
  { id: "zip", numeric: true, disablePadding: false, label: "Zip Code" },
];

function EnhancedTableHead(props) {
  const { classes, order, orderBy, onRequestSort } = props;
  const createSortHandler = (property) => (event) => {
    onRequestSort(event, property);
  };

  return (
    <TableHead>
      <TableRow>
        {headCells.map((headCell) => (
          <TableCell
            key={headCell.id}
            align={headCell.numeric ? "left" : "left"}
            padding={headCell.disablePadding ? "none" : "default"}
            sortDirection={orderBy === headCell.id ? order : false}
          >
            <TableSortLabel
              active={orderBy === headCell.id}
              direction={orderBy === headCell.id ? order : "asc"}
              onClick={createSortHandler(headCell.id)}
            >
              {headCell.label}
              {orderBy === headCell.id ? (
                <span className={classes.visuallyHidden}>
                  {order === "desc" ? "sorted descending" : "sorted ascending"}
                </span>
              ) : null}
            </TableSortLabel>
          </TableCell>
        ))}
      </TableRow>
    </TableHead>
  );
}

EnhancedTableHead.propTypes = {
  classes: PropTypes.object.isRequired,
  onRequestSort: PropTypes.func.isRequired,
  order: PropTypes.oneOf(["asc", "desc"]).isRequired,
  orderBy: PropTypes.string.isRequired,
  rowCount: PropTypes.number.isRequired,
};

const useStyles = makeStyles((theme) => ({
  root: {
    width: "100%",
  },
  paper: {
    width: "100%",
    marginBottom: theme.spacing(2),
  },
  table: {
    minWidth: 750,
  },
  visuallyHidden: {
    border: 0,
    clip: "rect(0 0 0 0)",
    height: 1,
    margin: -1,
    overflow: "hidden",
    padding: 0,
    position: "absolute",
    top: 20,
    width: 1,
  },
}));

/*********************************************************************************/
/******************************** TABLE OUTPUT FN ********************************/
/*********************************************************************************/

const Posts = ({ posts, loading, totalPosts }) => {
  const input = React.useRef();
  const classes = useStyles();

  const [order, setOrder] = React.useState("asc");
  const [orderBy, setOrderBy] = React.useState("name");

  const [page, setPage] = React.useState(0);
  const [rowsPerPage, setRowsPerPage] = React.useState(6);

  const emptyRows =
    rowsPerPage - Math.min(rowsPerPage, totalPosts - page * rowsPerPage);

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };

  const handleRequestSort = (event, property) => {
    const isAsc = orderBy === property && order === "asc";
    setOrder(isAsc ? "desc" : "asc");
    setOrderBy(property);
  };

  if (loading) {
    return (
      <Typography
        component="h2"
        variant="h2"
        align="center"
        color="textPrimary"
        gutterBottom
      >
        Loading...
      </Typography>
    );
  }

  // for each post ouput a table row
  return (
    <React.Fragment>
      <Typography
        component="h5"
        variant="h5"
        align="center"
        color="textPrimary"
        gutterBottom
      >
        Charitable Organizations
      </Typography>
      <Form
        className="justify-content-center"
        inline
        onSubmit={(e) => {
          e.preventDefault();
        }}
      >
        <br></br>
        <FormControl
          type="text"
          placeholder="Search"
          className="mr-sm-2"
          ref={input}
          onKeyPress={(event) => {
            if (event.key === "Enter") {
              window.location.href =
                "/search/q=" + input.current.value + "/filter=organizations";
            }
          }}
        />
        {/* <Button variant="outline-success" onClick={() => this.search()}>Search</Button> */}
      </Form>
      <FilterMenu />
      <TableContainer component={Paper}>
        <Table className={classes.table} aria-label="customized table">
          <EnhancedTableHead
            classes={classes}
            order={order}
            orderBy={orderBy}
            onRequestSort={handleRequestSort}
            rowCount={totalPosts}
          />
          <TableBody>
            {stableSort(posts, getComparator(order, orderBy))
              .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
              .map((post) => (
                <StyledTableRow key={post.id}>
                  <StyledTableCell
                    align="left"
                    scope="row"
                    component="a"
                    href={"/organizations/" + post.id}
                  >
                    {" "}
                    {post.name}
                  </StyledTableCell>
                  <StyledTableCell align="left">{post.income}</StyledTableCell>
                  <StyledTableCell align="left">{post.cause}</StyledTableCell>
                  <StyledTableCell align="left">{post.city}</StyledTableCell>
                  <StyledTableCell align="left">{post.zip}</StyledTableCell>
                </StyledTableRow>
              ))}
            {emptyRows > 0 && (
              <TableRow style={{ height: 53 * emptyRows }}>
                <TableCell colSpan={6} />
              </TableRow>
            )}
          </TableBody>
          <TableFooter>
            <TableRow>
              <TablePagination
                rowsPerPageOptions={[6, 9, 12, { label: "All", value: -1 }]}
                colSpan={3}
                count={totalPosts}
                rowsPerPage={rowsPerPage}
                page={page}
                SelectProps={{
                  inputProps: { "aria-label": "rows per page" },
                  native: true,
                }}
                onChangePage={handleChangePage}
                onChangeRowsPerPage={handleChangeRowsPerPage}
                ActionsComponent={TablePaginationActions}
              ></TablePagination>
            </TableRow>
          </TableFooter>
        </Table>
      </TableContainer>
    </React.Fragment>
  );
};

export default Posts;
