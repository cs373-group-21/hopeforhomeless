import React, { useEffect, useState } from "react";
import "../style.css";
import "../App.css";

import Noah from "../media/headshots/Noah_Image.jpeg";
import Shayan from "../media/headshots/Shayan_Image.jpg";
import Shikhar from "../media/headshots/Shikhar_Image.jpg";
import Sruthi from "../media/headshots/Sruthi_Image.jpg";
import Truman from "../media/headshots/Truman_Image.png";

import Teams from "../media/tools/teams.png";
import Postman from "../media/tools/postman.png";
import Gitlab from "../media/tools/gitlab.jpeg";
import NameCheap from "../media/tools/namecheap.png";
import AWS from "../media/tools/aws.png";
import ReactImage from "../media/tools/react.png";
import Postgres from "../media/tools/postgresql.png";
import Flask from "../media/tools/flask.png";
import AlgoliaImage from "../media/tools/algolia.png";
import ChartjsImage from "../media/tools/chartjs.png";

function About() {
  const [commitList, setItems3] = useState([]);
  const [authorsList, setItems4] = useState([]);
  const [closersList, setItems5] = useState([]);
  const [totalCommits, setItems6] = useState([]);
  const [totalIssues, setItems7] = useState([]);

  let commits = new Array(5).fill(0);
  let authors = new Array(5).fill(0);
  let closers = new Array(5).fill(0);
  const names =
    "Shikhar Gupta       Noah Galloso        Sruthi Rudravajjala Shayan Maradia      Truman Byrd";

  useEffect(() => {
    fetch(`https://gitlab.com/api/v4/projects/24704262/issues?per_page=1000`)
      .then((res) => res.json())
      .then((result) => {
        //setItems(result);
        let numIssues = 0;
        for (var issue in result) {
          numIssues++;
          let author = names.indexOf(String(result[issue].author.name));
          if (author !== -1) authors[author / 20]++;
          if (result[issue].state === "closed") {
            let closer = names.indexOf(String(result[issue].closed_by.name));
            if (closer !== -1) closers[closer / 20]++;
          }
        }
        setItems4(authors);
        setItems5(closers);
        setItems7(numIssues);
      });
  });

  useEffect(() => {
    fetch(
      `https://gitlab.com/api/v4/projects/24704262/repository/commits?per_page=10000`
    )
      .then((res) => res.json())
      .then((result) => {
        //setItems(result);
        let numCommits = 0;
        for (var commit in result) {
          numCommits++;
          let author = names.indexOf(String(result[commit].author_name));
          if (author !== -1) commits[author / 20]++;
          else if (result[commit].committer_name === "EC2 Default User")
            commits[0]++;
        }
        setItems3(commits);
        setItems6(numCommits);
      });
  });

  return (
    <div data-testid="About" className="About">
      <header>
        <div id="navigation"></div>
      </header>

      <main>
        <script src="/src/.env.js"></script>
        <script src="/src/gitlab.js"></script>
        <div className="content">
          <h1>About</h1>
          <div className="descriptionContainer">
            <h3>Our Mission</h3>
            <p>
              Hope for Homeless's mission is threefold: we seek to educate the
              general public about the state of homelessness and poverty in
              major cities, provide information about available organizations
              that can assist those in need, and help families in need find
              quality public schools in their area. There is a serious lack of
              information about the impoverished population as well as available
              resources in major cities, and we hope to bridge this gap.
            </p>
          </div>
          <div className="container">
            <h3>Meet the Team</h3>
          </div>
          <div className="cardFlexContainer" styles="margin-top: 0px;">
            <div className="row">
              <div className="col-auto">
                <div className="card">
                  <div className="ant-card ant-card-bordered ant-card-hoverable Splash_card__1rIR5">
                    <div className="row">
                      <h2 className="ant-typography">Shikhar Gupta</h2>
                    </div>
                    <div className="row">
                      <div className="ant-card-cover">
                        <img
                          className="cardImage"
                          alt="Shikhar"
                          src={Shikhar}
                        />
                      </div>
                    </div>
                    <div className="row">
                      <div className="descriptionContainer">
                        <p>
                          Shikhar has a passion for alleviating poverty, is a
                          sophomore, and loves programming!
                        </p>
                        <p>
                          Shikhar's major responsibilities included working on
                          the full stack.
                        </p>
                        <p>Commits: {commitList[0]}</p>
                        <p>Issues Authored: {authorsList[0]}</p>
                        <p>Issues Closed: {closersList[0]}</p>
                        <p>Tests: 16</p>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="col-auto">
              <div className="card">
                <div className="ant-card ant-card-bordered ant-card-hoverable Splash_card__1rIR5">
                  <div className="row">
                    <h2 className="ant-typography">Sruthi Rudravajjala</h2>
                  </div>
                  <div className="row">
                    <div className="ant-card-cover">
                      <img className="cardImage" alt="Sruthi" src={Sruthi} />
                    </div>
                  </div>
                  <div className="row">
                    <div className="descriptionContainer">
                      <p>
                        Sruthi has a passion for community service and helping
                        people in need, is a Junior, and loves technology!
                      </p>
                      <p>
                        Sruthi's major responsibilities included working on the
                        front-end.
                      </p>
                      <p>Commits: {commitList[2]}</p>
                      <p>Issues Authored: {authorsList[2]}</p>
                      <p>Issues Closed: {closersList[2]}</p>
                      <p>Tests: 3</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="col-auto">
              <div className="card">
                <div className="ant-card ant-card-bordered ant-card-hoverable Splash_card__1rIR5">
                  <div className="row">
                    <h2 className="ant-typography">Truman Byrd</h2>
                  </div>
                  <div className="row">
                    <div className="ant-card-cover">
                      <img className="cardImage" alt="Truman" src={Truman} />
                    </div>
                  </div>
                  <div className="row">
                    <div className="descriptionContainer">
                      <p>
                        Truman is passionate about helping homeless people, is a
                        Junior, and likes to skateboard!
                      </p>
                      <p>
                        Truman's major responsibilities included working on the
                        back-end.
                      </p>
                      <p>Commits: {commitList[4]}</p>
                      <p>Issues Authored: {authorsList[4]}</p>
                      <p>Issues Closed: {closersList[4]}</p>
                      <p>Tests: 25</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="col-auto">
              <div className="card">
                <div className="ant-card ant-card-bordered ant-card-hoverable Splash_card__1rIR5">
                  <div className="row">
                    <h2 className="ant-typography">Noah Galloso</h2>
                  </div>
                  <div className="row">
                    <div className="ant-card-cover">
                      <img className="cardImage" alt="Noah" src={Noah} />
                    </div>
                  </div>
                  <div className="row">
                    <div className="descriptionContainer">
                      <p>
                        Noah is passionate about helping people who are less
                        fortunate, is a Senior, and plays the guitar!
                      </p>
                      <p>
                        Noah's major responsibilities included working on the
                        back-end.
                      </p>
                      <p>Commits: {commitList[1]}</p>
                      <p>Issues Authored: {authorsList[1]}</p>
                      <p>Issues Closed: {closersList[1]}</p>
                      <p>Tests: 25</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="col-auto">
              <div className="card">
                <div className="ant-card ant-card-bordered ant-card-hoverable Splash_card__1rIR5">
                  <div className="row">
                    <h2 className="ant-typography">Shayan Maradia</h2>
                  </div>
                  <div className="row">
                    <div className="ant-card-cover">
                      <img className="cardImage" alt="Shayan" src={Shayan} />
                    </div>
                  </div>
                  <div className="row">
                    <div className="descriptionContainer">
                      <p>
                        Shayan is passionate about improving the quality of life
                        of impoverished communities, is a junior, and likes to
                        play basketball!
                      </p>
                      <p>
                        Shayan's major responsibilities included working on the
                        front-end.
                      </p>
                      <p>Commits: {commitList[3]}</p>
                      <p>Issues Authored: {authorsList[3]}</p>
                      <p>Issues Closed: {closersList[3]}</p>
                      <p>Tests: 16</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="container" styles="margin-top: 2em;">
            <br></br>
            <h3>Data</h3>
            <table className="table table-striped">
              <thead>
                <tr>
                  <th scope="col">Source</th>
                  <th scope="col">Description</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>
                    <a href="https://charity.3scale.net/">
                      CharityNavigator API
                    </a>
                  </td>
                  <td>
                    Used to gather information about charitable organizations in
                    various cities.
                  </td>
                </tr>
                <tr>
                  <td>
                    <a href="https://api.census.gov/data/timeseries/poverty/saipe.html">
                      Census API
                    </a>
                  </td>
                  <td>
                    Used to compile income and poverty statistics for various
                    cities around the country.
                  </td>
                </tr>
                <tr>
                  <td>
                    <a href="https://developer.schooldigger.com/">
                      SchoolDigger API
                    </a>
                  </td>
                  <td>
                    Used to display public school statistics in various cities.
                  </td>
                </tr>
                <tr>
                  <td>
                    <a href="https://serpapi.com/maps-local-results">
                      Google Maps API
                    </a>
                  </td>
                  <td>Used to render maps for all instances.</td>
                </tr>
                <tr>
                  <td>
                    <a href="https://serpapi.com/youtube-search-api">
                      Youtube API
                    </a>
                  </td>
                  <td>
                    Used to search for videos to embed in school and city pages.
                  </td>
                </tr>
              </tbody>
            </table>
          </div>
          <div className="container">
            <h3>Tools</h3>
          </div>
          <div className="cardFlexContainer">
            <div className="row">
              <div className="col-auto">
                <a className="card card-small" href="https://aws.amazon.com/">
                  <div className="ant-card-cover">
                    <img className="cardImage" src={AWS} alt="aws" />
                  </div>
                  <div className="card-body">
                    <h5 className="card-title">AWS</h5>
                    <p className="card-text">
                      Deploy, manage the database, and route traffic.
                    </p>
                  </div>
                </a>
              </div>
              <div className="col-auto">
                <a
                  className="card card-small"
                  href="https://www.namecheap.com/"
                >
                  <div className="ant-card-cover">
                    <img
                      className="cardImage"
                      src={NameCheap}
                      alt="namecheap"
                    />
                  </div>
                  <div className="card-body">
                    <h5 className="card-title">NameCheap</h5>
                    <p className="card-text">Register the domain.</p>
                  </div>
                </a>
              </div>
              <div className="col-auto">
                <a className="card card-small" href="https://www.gitlab.com/">
                  <div className="ant-card-cover">
                    <img className="cardImage" src={Gitlab} alt="gitlab" />
                  </div>
                  <div className="card-body">
                    <h5 className="card-title">GitLab</h5>
                    <p className="card-text">
                      Hosts the source code for the website.
                    </p>
                  </div>
                </a>
              </div>
              <div className="col-auto">
                <a className="card card-small" href="https://www.postman.com/">
                  <div className="ant-card-cover">
                    <img className="cardImage" src={Postman} alt="postman" />
                  </div>
                  <div className="card-body">
                    <h5 className="card-title">Postman</h5>
                    <p className="card-text">
                      Define and implement the API for the backend.
                    </p>
                  </div>
                </a>
              </div>

              <div className="col-auto">
                <a
                  className="card card-small"
                  href="https://flask.palletsprojects.com/en/1.1.x/"
                >
                  <div className="ant-card-cover">
                    <img className="cardImage" src={Flask} alt="Flask" />
                  </div>
                  <div className="card-body">
                    <h5 className="card-title">Flask</h5>
                    <p className="card-text">
                      Python script to interface between database and website
                    </p>
                  </div>
                </a>
              </div>

              <div className="col-auto">
                <a
                  className="card card-small"
                  href="https://www.postgresql.org/"
                >
                  <div className="ant-card-cover">
                    <img
                      className="cardImage"
                      src={Postgres}
                      alt="PostgreSQL"
                    />
                  </div>
                  <div className="card-body">
                    <h5 className="card-title">PostgreSQL</h5>
                    <p className="card-text">Used for the database</p>
                  </div>
                </a>
              </div>

              <div className="col-auto">
                <a className="card card-small" href="https://reactjs.org/">
                  <div className="ant-card-cover">
                    <img className="cardImage" src={ReactImage} alt="React" />
                  </div>
                  <div className="card-body">
                    <h5 className="card-title">React</h5>
                    <p className="card-text">
                      Used for front-end of the website
                    </p>
                  </div>
                </a>
              </div>

              <div className="col-auto">
                <a className="card card-small" href="https://www.chartjs.org/">
                  <div className="ant-card-cover">
                    <img className="cardImage" src={ChartjsImage} alt="teams" />
                  </div>
                  <div className="card-body">
                    <h5 className="card-title">Chart.js</h5>
                    <p className="card-text">Used to create visualizations.</p>
                  </div>
                </a>
              </div>

              <div className="col-auto">
                <a className="card card-small" href="https://www.algolia.com/">
                  <div className="ant-card-cover">
                    <img
                      className="cardImage"
                      src={AlgoliaImage}
                      alt="Algolia"
                    />
                  </div>
                  <div className="card-body">
                    <h5 className="card-title">Algolia</h5>
                    <p className="card-text">
                      Used to implement search features
                    </p>
                  </div>
                </a>
              </div>
              <div className="col-auto">
                <a
                  className="card card-small"
                  href="https://www.microsoft.com/en-us/microsoft-teams/group-chat-software"
                >
                  <div className="ant-card-cover">
                    <img className="cardImage" src={Teams} alt="teams" />
                  </div>
                  <div className="card-body">
                    <h5 className="card-title">Microsoft Teams</h5>
                    <p className="card-text">Used for team communication.</p>
                  </div>
                </a>
              </div>
            </div>
          </div>
          <div className="descriptionContainer">
            <h3>Statistics</h3>
            <h5 id="commit">Total Commits: {totalCommits}</h5>
            <h5 id="issues">Total Issues: {totalIssues}</h5>
          </div>
          <div className="API Documentation">
            <h5>
              <a href="https://documenter.getpostman.com/view/14728622/TzJrDerL">
                Postman API Documentation
              </a>
            </h5>
          </div>
          <div className="GitLab Repository">
            <h5>
              <a href="https://gitlab.com/cs373-group-21/hopeforhomeless">
                GitLab Repository
              </a>
            </h5>
          </div>
          <h5>
            <a href="https://youtu.be/IVNiE11qgAM">Final Presentation</a>
          </h5>
        </div>
      </main>
      <footer></footer>
      <script
        src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-b5kHyXgcpbZJO/tY9Ul7kGkf1S0CWuKcCD38l8YkeH8z8QjE0GmW1gYU5S9FOnJ0"
        crossOrigin="anonymous"
      ></script>
    </div>
  );
}
export default About;
