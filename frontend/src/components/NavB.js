// Using Burnin Up project as a reference
import React from "react";
import "../style.css";
import "../App.css";
import Logo from "../media/favicon/logo_inverted.png";
// Used Burnin Up as reference for search functions
import Navbar from "react-bootstrap/Navbar";
import Nav from "react-bootstrap/Nav";
import Form from "react-bootstrap/Form";
import FormControl from "react-bootstrap/FormControl";
import Button from "react-bootstrap/Button";

// now capitalized
class NavB extends React.Component {
  constructor(props) {
    super(props);
    // create a ref to store the textInput DOM element
    this.textInput = React.createRef();
  }

  search() {
    window.location.href =
      "/search/q=" + this.textInput.current.value + "/filter=all";
  }

  render() {
    return (
      // https://react-bootstrap.github.io/components/navbar/
      <Navbar bg="dark" expand="lg" variant="dark">
        <Navbar.Brand href="/">
          <img
            alt="logo"
            src={Logo}
            width="40"
            height="40"
            className="d-inline-block"
          />{" "}
          Hope For Homeless
        </Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="responsive-navbar-nav">
          <Nav className="mr-auto">
            <Nav.Link href="/about">About</Nav.Link>
            <Nav.Link href="/organizations/n=/ct=/ca=/z=/i=">
              Organizations
            </Nav.Link>
            <Nav.Link href="/locations/n=/s=/po=/pv=/i=">Cities</Nav.Link>
            <Nav.Link href="/schools/n=/s=/d=/l=/r=">Schools</Nav.Link>
            <Nav.Link href="/visualizations/hfh/cities">
              Visualizations
            </Nav.Link>
            <Nav.Link href="/visualizations/provider/countries">
              Provider Visualizations
            </Nav.Link>
          </Nav>
          <Form
            className="justify-content-center"
            inline
            onSubmit={(e) => {
              e.preventDefault();
            }}
          >
            <FormControl
              type="text"
              placeholder="Search"
              className="mr-sm-2"
              ref={this.textInput}
              onKeyPress={(event) => {
                if (event.key === "Enter") {
                  this.search();
                }
              }}
            />
            <Button variant="outline-success" onClick={() => this.search()}>
              Search
            </Button>
          </Form>
        </Navbar.Collapse>
      </Navbar>
    );
  }
}
export default NavB;
