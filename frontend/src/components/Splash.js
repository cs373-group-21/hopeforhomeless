// Source: ui material
import React from "react";
import Button from "@material-ui/core/Button";
import Card from "@material-ui/core/Card";
import CardActions from "@material-ui/core/CardActions";
import CardContent from "@material-ui/core/CardContent";
import CardMedia from "@material-ui/core/CardMedia";
import CssBaseline from "@material-ui/core/CssBaseline";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import { makeStyles } from "@material-ui/core/styles";
import Container from "@material-ui/core/Container";
import Location_Picture from "../media/splash_page_images/Austin_Skyline.jpg";
import Location_Picture2 from "../media/splash_page_images/hands.jpg";
import Location_Picture3 from "../media/splash_page_images/school.jpg";

const useStyles = makeStyles((theme) => ({
  icon: {
    marginRight: theme.spacing(2),
  },
  heroContent: {
    backgroundColor: theme.palette.background.paper,
    padding: theme.spacing(8, 0, 6),
  },
  heroButtons: {
    marginTop: theme.spacing(4),
    justifyContent: "center",
  },
  cardGrid: {
    paddingTop: theme.spacing(8),
    paddingBottom: theme.spacing(8),
  },
  card: {
    height: "100%",
    display: "flex",
    flexDirection: "column",
  },
  cardMedia: {
    paddingTop: "56.25%", // 16:9
  },
  cardContent: {
    flexGrow: 1,
  },
}));

const data = [
  {
    pic: Location_Picture,
    model: "Location",
    link: "/locations/n=/s=/po=/pv=/i=",
    description:
      "Want to see your city's stats? Compare all the major U.S. cities.",
  },
  {
    pic: Location_Picture2,
    model: "Charities",
    link: "/organizations/n=/ct=/ca=/z=/i=",
    description:
      "What resources are available? Check out organizations making a difference!",
  },
  {
    pic: Location_Picture3,
    model: "Education",
    link: "/schools/n=/s=/d=/l=/r=",
    description:
      "Learn more about the schools and education in different areas!",
  },
];

export default function Album() {
  const classes = useStyles();

  return (
    <React.Fragment>
      <CssBaseline />
      <main>
        {/* Hero unit */}
        <div className="content">
          <div className={classes.heroContent}>
            <Container maxWidth="sm">
              <Typography
                component="h5"
                variant="h5"
                align="center"
                color="textSecondary"
                gutterBottom
              >
                MAKE A DIFFERENCE
              </Typography>
              <Typography
                component="h1"
                variant="h2"
                align="center"
                color="textPrimary"
                gutterBottom
              >
                Hope for Homeless
              </Typography>
              <Typography
                variant="h5"
                align="center"
                color="textSecondary"
                paragraph
              >
                Lend a helping hand. Find information about homelessness, your
                area's education statistics, and charitable organizations and
                resources to help.
              </Typography>
            </Container>
          </div>
          <Container className={classes.cardGrid} maxWidth="md">
            <Grid container spacing={4}>
              {data.map((elem) => (
                <Grid item key={data.indexOf(elem)} xs={12} sm={6} md={4}>
                  <Card className={classes.card}>
                    <CardMedia
                      className={classes.cardMedia}
                      image={elem.pic}
                      title={elem.model}
                    />
                    <CardContent className={classes.cardContent}>
                      <Typography gutterBottom variant="h5" component="h2">
                        {elem.model}
                      </Typography>
                      <Typography>{elem.description}</Typography>
                    </CardContent>
                    <CardActions style={{ justifyContent: "center" }}>
                      <Button
                        size="small"
                        color="primary"
                        align="center"
                        href={elem.link}
                      >
                        Learn More
                      </Button>
                    </CardActions>
                  </Card>
                </Grid>
              ))}
            </Grid>
          </Container>
        </div>
      </main>
    </React.Fragment>
  );
}
