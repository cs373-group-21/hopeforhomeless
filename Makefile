.DEFAULT_GOAL := all
MAKEFLAGS += --no-builtin-rules
SHELL         := bash

all:

test:
	make backend-test
	make frontend-test
	make postman
	make selenium

backend-run:
	$(MAKE) -C backend/ run

backend-test:
	$(MAKE) -C backend/ clean
	$(MAKE) -C backend/ test

frontend-run:
	$(MAKE) -C frontend/ run

frontend-test:
	$(MAKE) -C frontend/ test

frontend-deploy:
	$(MAKE) -C frontend/ deploy
	cp frontend/.htaccess frontend/build/
	sudo systemctl restart httpd
	cd ..

postman:
	npm install -g newman
	cd postman_tests
	npm install -g newman-reporter-html
	cd ..
	$(MAKE) -C postman_tests/ clean
	$(MAKE) -C postman_tests/ test

selenium:
	$(MAKE) -C selenium_tests/ test


