#!/usr/bin/python3
import sys
import os
def application(environ, start_response):
    # explicitly set environment variables from the WSGI-supplied ones
    ENVIRONMENT_VARIABLES = [
        'POSTGRES_USER',
        'POSTGRES_PASS',
        'POSTGRES_URL',
        'POSTGRES_DB'
    ]
    for key in ENVIRONMENT_VARIABLES:
        os.environ[key] = environ.get(key)
 
    from main import app

    return app(environ, start_response)
