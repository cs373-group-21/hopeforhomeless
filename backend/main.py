import os
from flask import Flask, jsonify, request
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
from flask_marshmallow import Marshmallow, Schema, fields
from flask_cors import CORS
from sqlalchemy import and_, or_, not_
import psycopg2

app = Flask(__name__)
CORS(app)

app.config["SQLALCHEMY_DATABASE_URI"] = (
    f'postgresql+psycopg2://{os.getenv("POSTGRES_USER")}:'
    + f'{os.getenv("POSTGRES_PASS")}@{os.getenv("POSTGRES_URL")}/{os.getenv("POSTGRES_DB")}'
)

app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False

db = SQLAlchemy(app)
migrate = Migrate(app, db)
ma = Marshmallow(app)

cityKeys = {"state"}
orgKeys = {"city", "state", "cityId"}
schoolKeys = {"city", "state", "cityId"}


class Organization(db.Model):
    __tablename__ = "organizations"

    id = db.Column(db.Integer, primary_key=True)
    cityId = db.Column(db.Integer, db.ForeignKey("city.id"))
    name = db.Column(db.String())
    cause = db.Column(db.String())
    city = db.Column(db.String())
    state = db.Column(db.String())
    zip = db.Column(db.Integer)
    assets = db.Column(db.Integer)
    income = db.Column(db.Integer)
    irs_class = db.Column(db.String())
    rating = db.Column(db.Float)
    ein = db.Column(db.Integer)
    tagline = db.Column(db.String())
    url = db.Column(db.String())
    address = db.Column(db.String())
    mission = db.Column(db.String())
    map = db.Column(db.String())
    image = db.Column(db.String())

    def __init__(
        self,
        id,
        cityId,
        name,
        cause,
        city,
        state,
        zip,
        assets,
        income,
        irs_class,
        rating,
        ein,
        tagline,
        url,
        address,
        mission,
        map,
        image,
    ):
        self.id = id
        self.cityId = cityId
        self.name = name
        self.cause = cause
        self.city = city
        self.state = state
        self.zip = zip
        self.assets = assets
        self.income = income
        self.irs_class = irs_class
        self.rating = rating
        self.ein = ein
        self.tagline = tagline
        self.url = url
        self.address = address
        self.mission = mission
        self.map = map
        self.image = image


class City(db.Model):
    __tablename__ = "city"

    id = db.Column(db.Integer, primary_key=True)
    city = db.Column(db.String())
    state = db.Column(db.String())
    totalPopulation = db.Column(db.Integer)
    medianIncome = db.Column(db.Integer)
    povertyPopulation = db.Column(db.Integer)
    unemployed = db.Column(db.Integer)
    medianRent = db.Column(db.Integer)
    vacantHousingPercent = db.Column(db.Float)
    foodStampsPercent = db.Column(db.Float)
    collegeDegreePercent = db.Column(db.Float)
    percentHealthInsured = db.Column(db.Float)
    map = db.Column(db.String())
    video = db.Column(db.String())
    image = db.Column(db.String())
    schoolVideo = db.Column(db.String())

    def __init__(
        self,
        id,
        city,
        state,
        totalPopulation,
        medianIncome,
        povertyPopulation,
        unemployed,
        medianRent,
        vacantHousingPercent,
        foodStampsPercent,
        collegeDegreePercent,
        percentHealthInsured,
        map,
        video,
        image,
        schoolVideo,
    ):
        self.id = id
        self.city = city
        self.state = state
        self.totalPopulation = totalPopulation
        self.medianIncome = medianIncome
        self.povertyPopulation = povertyPopulation
        self.unemployed = unemployed
        self.medianRent = medianRent
        self.vacantHousingPercent = vacantHousingPercent
        self.foodStampsPercent = foodStampsPercent
        self.collegeDegreePercent = collegeDegreePercent
        self.percentHealthInsured = percentHealthInsured
        self.map = map
        self.video = video
        self.image = image
        self.schoolVideo = schoolVideo


class School(db.Model):
    __tablename__ = "school"

    id = db.Column(db.Integer, primary_key=True)
    cityId = db.Column(db.Integer, db.ForeignKey("city.id"))
    city = db.Column(db.String())
    state = db.Column(db.String())
    schoolName = db.Column(db.String())
    district = db.Column(db.String())
    schoolLevel = db.Column(db.String())
    rankPercentile = db.Column(db.Float)
    numStudents = db.Column(db.Integer)
    percentFreeLunch = db.Column(db.Float)
    studentTeacherRatio = db.Column(db.Float)
    pctAfricanAmerican = db.Column(db.Float)
    pctAsian = db.Column(db.Float)
    pctHispanic = db.Column(db.Float)
    pctIndian = db.Column(db.Float)
    pctPacificIslander = db.Column(db.Float)
    pctWhite = db.Column(db.Float)
    pctMixed = db.Column(db.Float)
    address = db.Column(db.String())
    latitude = db.Column(db.Float)
    longitude = db.Column(db.Float)
    map = db.Column(db.String())
    video = db.Column(db.String())

    def __init__(
        self,
        id,
        cityId,
        city,
        state,
        schoolName,
        district,
        schoolLevel,
        rankPercentile,
        numStudents,
        percentFreeLunch,
        studentTeacherRatio,
        pctAfricanAmerican,
        pctAsian,
        pctHispanic,
        pctIndian,
        pctPacificIslander,
        pctWhite,
        pctMixed,
        address,
        latitude,
        longitude,
        map,
        video,
    ):
        self.id = id
        self.cityId = cityId
        self.city = city
        self.state = state
        self.schoolName = schoolName
        self.district = district
        self.schoolLevel = schoolLevel
        self.rankPercentile = rankPercentile
        self.numStudents = numStudents
        self.percentFreeLunch = percentFreeLunch
        self.studentTeacherRatio = studentTeacherRatio
        self.pctAfricanAmerican = pctAfricanAmerican
        self.pctAsian = pctAsian
        self.pctHispanic = pctHispanic
        self.pctIndian = pctIndian
        self.pctPacificIslander = pctPacificIslander
        self.pctWhite = pctWhite
        self.pctMixed = pctMixed
        self.address = address
        self.latitude = latitude
        self.longitude = longitude
        self.map = map
        self.video = video


class OrganizationSchema(Schema):
    class Meta:
        ordered = True
        fields = (
            "id",
            "cityId",
            "name",
            "cause",
            "city",
            "state",
            "zip",
            "assets",
            "income",
            "irs_class",
            "rating",
            "ein",
            "tagline",
            "url",
            "address",
            "mission",
            "map",
            "image",
        )
        model = Organization


organization_schema = OrganizationSchema()
organizations_schema = OrganizationSchema(many=True)


class SchoolSchema(Schema):
    class Meta:
        ordered = True
        fields = (
            "id",
            "cityId",
            "city",
            "state",
            "schoolName",
            "district",
            "schoolLevel",
            "rankPercentile",
            "numStudents",
            "percentFreeLunch",
            "studentTeacherRatio",
            "pctAfricanAmerican",
            "pctAsian",
            "pctHispanic",
            "pctIndian",
            "pctPacificIslander",
            "pctWhite",
            "pctMixed",
            "address",
            "latitude",
            "longitude",
            "map",
            "video",
        )
        model = School


school_schema = SchoolSchema()
schools_schema = SchoolSchema(many=True)


class CitySchema(Schema):
    class Meta:
        ordered = True
        fields = (
            "id",
            "city",
            "state",
            "totalPopulation",
            "medianIncome",
            "povertyPopulation",
            "unemployed",
            "medianRent",
            "vacantHousingPercent",
            "foodStampsPercent",
            "collegeDegreePercent",
            "percentHealthInsured",
            "map",
            "video",
            "image",
        )
        model = City


city_schema = CitySchema()
cities_schema = CitySchema(many=True)


@app.route("/")
def index():
    return """<h1>HopeForHomeless API</h1>
    <p>You've reached the HopeForHomeless API Base Link. Please specify parameters to receive a JSON response.</p>"""


@app.route("/cities")
def getCities():
    fcity = []
    fstate = []
    fpopulation = []
    fmedianincome = []
    fpoverty = []
    # city filter alphabetical
    if request.args.get("fcity") != None:
        nameFilter = request.args.get("fcity")
        for i in nameFilter:
            if i == 'a':
                fcity.extend(City.city.like(f'{chr(i)}%') for i in range(ord('A'), ord('I') + 1))
            elif i == 'j':
                fcity.extend(City.city.like(f'{chr(i)}%') for i in range(ord('J'), ord('Q') + 1))
            elif i == 'r':
                fcity.extend(City.city.like(f'{chr(i)}%') for i in range(ord('R'), ord('Z') + 1))

    # state filter alphabetical
    if request.args.get("fstate") != None:
        stateFilter = request.args.get("fstate")
        for i in stateFilter:
            if i == 'a':
                fstate.extend(City.state.like(f'{chr(i)}%') for i in range(ord('A'), ord('I') + 1))
            elif i == 'j':
                fstate.extend(City.state.like(f'{chr(i)}%') for i in range(ord('J'), ord('Q') + 1))
            elif i == 'r':
                fstate.extend(City.state.like(f'{chr(i)}%') for i in range(ord('R'), ord('Z') + 1))

    # population
    if request.args.get("fpopulation") != None:
        popFilter = request.args.get("fpopulation")
        for i in popFilter:
            if i == '1':
                fpopulation.append(City.totalPopulation.between(0, 250_000))
            elif i == '2':
                fpopulation.append(City.totalPopulation.between(250_001, 600_000))
            elif i == '3':
                fpopulation.append(City.totalPopulation.between(600_000, 10_000_000))

    # medianincome
    if request.args.get("fmedianincome") != None:
        incomeFilter = request.args.get("fmedianincome")
        for i in str(incomeFilter):
            if i == '1':
                fmedianincome.append(City.medianIncome.between(0, 50_000))
            elif i == '2':
                fmedianincome.append(City.medianIncome.between(50_001, 80_000))
            elif i == '3':
                fmedianincome.append(City.medianIncome.between(80_001, 150_000))

    # poverty
    if request.args.get("fpoverty") != None:
        povertyFilter = request.args.get("fpoverty")
        for i in str(povertyFilter):
            if i == '1':
                fpoverty.append(City.povertyPopulation.between(0, 40_000))
            elif i == '2':
                fpoverty.append(City.povertyPopulation.between(40_001, 90_000))
            elif i == '3':
                fpoverty.append(City.povertyPopulation.between(90_000, 2_000_000))


    matches = db.session.query(City).filter(or_(*fcity)).filter(or_(*fstate)).filter(or_(*fpoverty)).filter(or_(*fmedianincome)).filter(or_(*fpopulation))
    if request.args.get("page") != None:
        page = int(request.args.get("page"))
        matches = matches.paginate(page=page, per_page=20).items
    else:
        matches = matches.all()
    return jsonify(cities_schema.dump(matches))


@app.route("/cities/<int:id>")
def getCity(id):
    city = City.query.get(id)
    return city_schema.dump(city)


@app.route("/schools")

def getSchools():
    fname = []
    fstate = []
    fdistrict = []
    flevel = []
    frank = []
    # name filter alphabetical
    if request.args.get("fname") != None:
        nameFilter = request.args.get("fname")
        for i in nameFilter:
            if i == 'a':
                fname.extend(School.schoolName.like(f'{chr(i)}%') for i in range(ord('A'), ord('I') + 1))
            elif i == 'j':
                fname.extend(School.schoolName.like(f'{chr(i)}%') for i in range(ord('J'), ord('Q') + 1))
            elif i == 'r':
                fname.extend(School.schoolName.like(f'{chr(i)}%') for i in range(ord('R'), ord('Z') + 1))

    # state filter alphabetical
    if request.args.get("fstate") != None:
        stateFilter = request.args.get("fstate")
        for i in stateFilter:
            if i == 'a':
                fstate.extend(School.state.like(f'{chr(i)}%') for i in range(ord('A'), ord('I') + 1))
            elif i == 'j':
                fstate.extend(School.state.like(f'{chr(i)}%') for i in range(ord('J'), ord('Q') + 1))
            elif i == 'r':
                fstate.extend(School.state.like(f'{chr(i)}%') for i in range(ord('R'), ord('Z') + 1))

    # district filter alphabetical
    if request.args.get("fdistrict") != None:
        districtFilter = request.args.get("fdistrict")
        for i in districtFilter:
            if i == 'a':
                fdistrict.extend(School.district.like(f'{chr(i)}%') for i in range(ord('A'), ord('I') + 1))
            elif i == 'j':
                fdistrict.extend(School.district.like(f'{chr(i)}%') for i in range(ord('J'), ord('Q') + 1))
            elif i == 'r':
                fdistrict.extend(School.district.like(f'{chr(i)}%') for i in range(ord('R'), ord('Z') + 1))

    # level
    if request.args.get("flevel") != None:
        levelFilter = request.args.get("flevel")
        for i in str(levelFilter):
            if i == 'm':
                flevel.append(School.schoolLevel == "Middle")
            elif i == 'e':
                flevel.append(School.schoolLevel == "Elementary")
            elif i == 'h':
                flevel.append(School.schoolLevel == "High")

    # rank
    if request.args.get("frank") != None:
        rankFilter = request.args.get("frank")
        for i in str(rankFilter):
            if i == '1':
                frank.append(School.rankPercentile.between(0, 30))
            elif i == '2':
                frank.append(School.rankPercentile.between(31, 60))
            elif i == '3':
                frank.append(School.rankPercentile.between(61, 100))

    matches = db.session.query(School).filter(or_(*fname)).filter(or_(*fstate)).filter(or_(*fdistrict)).filter(or_(*flevel)).filter(or_(*frank))

    if request.args.get("cityId"):
        matches = matches.filter(School.cityId == request.args.get("cityId"))

    if request.args.get("page") != None:
        page = int(request.args.get("page"))
        matches = matches.paginate(page=page, per_page=100).items
    else:
        matches = matches.all()
    return jsonify(schools_schema.dump(matches))


@app.route("/schools/<int:id>")
def getSchool(id):
    school = School.query.get(id)
    return school_schema.dump(school)


@app.route("/organizations")
def getOrgs():
    fname = []
    fcat = []
    fcity = []
    fincome = []
    fzip = []
    # name filter alphabetical
    if request.args.get("fname") != None:
        nameFilter = request.args.get("fname")
        for i in nameFilter:
            if i == 'a':
                fname.extend(Organization.name.like(f'{chr(i)}%') for i in range(ord('A'),ord('I')+1))
            elif i == 'j':
                fname.extend(Organization.name.like(f'{chr(i)}%') for i in range(ord('J'),ord('Q')+1))
            elif i == 'r':
                fname.extend(Organization.name.like(f'{chr(i)}%') for i in range(ord('R'), ord('Z') + 1))

    # category filter alphabetical
    if request.args.get("fcause") != None:
        catFilter = request.args.get("fcause")
        for i in catFilter:
            if i == 'a':
                fcat.extend(Organization.cause.like(f'{chr(i)}%') for i in range(ord('A'),ord('I')+1))
            elif i == 'j':
                fcat.extend(Organization.cause.like(f'{chr(i)}%') for i in range(ord('J'),ord('Q')+1))
            elif i == 'r':
                fcat.extend(Organization.cause.like(f'{chr(i)}%') for i in range(ord('R'), ord('Z') + 1))

    # city filter alphabetical
    if request.args.get("fcity") != None:
        cityFilter = request.args.get("fcity")
        for i in cityFilter:
            if i == 'a':
                fcity.extend(Organization.city.like(f'{chr(i)}%') for i in range(ord('A'),ord('I')+1))
            elif i == 'j':
                fcity.extend(Organization.city.like(f'{chr(i)}%') for i in range(ord('J'),ord('Q')+1))
            elif i == 'r':
                fcity.extend(Organization.city.like(f'{chr(i)}%') for i in range(ord('R'), ord('Z') + 1))

    # income ranges
    if request.args.get("fincome") != None:
        incomeFilter = request.args.get("fincome")
        for i in str(incomeFilter):
            if i == '1':
                fincome.append(Organization.income.between(0, 3_000_000))
            elif i == '2':
                fincome.append(Organization.income.between(3_000_001, 10_000_000))
            elif i == '3':
                fincome.append(Organization.income.between(10_000_001, 1_000_000_000))

    # zip code
    if request.args.get("fzip") != None:
        zipFilter = request.args.get("fzip")
        for i in str(zipFilter):
            if i == '1':
                fzip.append(Organization.zip.between(0, 30000))
            elif i == '2':
                fzip.append(Organization.zip.between(30001, 60000))
            elif i == '3':
                fzip.append(Organization.zip.between(60000, 100000))

    matches = db.session.query(Organization).filter(or_(*fname)).filter(or_(*fcat)).filter(or_(*fcity)).filter(or_(*fincome)).filter(or_(*fzip))
    if request.args.get("cityId"):
        matches = matches.filter(Organization.cityId == request.args.get("cityId"))

    if request.args.get("page") != None:
        page = int(request.args.get("page"))
        matches = matches.paginate(page=page, per_page=100).items
    else:
        matches = matches.all()
    return jsonify(organizations_schema.dump(matches))


@app.route("/organizations/<int:id>")
def getOrg(id):
    org = Organization.query.get(id)
    return organization_schema.dump(org)


if __name__ == "__main__":
    app.run(host="0.0.0.0", port=5000)
