import unittest
import xmlrunner
from flask_testing import TestCase
import json
from main import *


class BaseTest(TestCase):

    # Configure mock database
    def create_app(self):
        app.config["DEBUG"] = True
        app.config["TESTING"] = True
        app.config["WTF_CSRF_ENABLED"] = False
        app.config["SQLALCHEMY_DATABASE_URI"] = "sqlite:///:memory:"
        return app

    # Add data to test database
    def setUp(self):
        db.create_all()
        # Add test cities
        db.session.add(
            City(
                1,
                "FakeCity",
                "TX",
                2_500_000,
                2000,
                5000,
                500,
                800,
                3,
                8,
                76,
                98,
                "exampleMap",
                "exampleVideo",
                "exampleImage",
                "exampleVideo",
            )
        )
        db.session.add(
            City(
                2,
                "FakeCity2",
                "AL",
                1_000_000,
                200,
                4,
                234,
                567,
                4,
                8,
                53,
                92,
                "exampleMap2",
                "exampleVideo2",
                "exampleImage2",
                "exampleVideo2",
            )
        )
        db.session.add(
            City(
                3,
                "FakeCity3",
                "TX",
                5200,
                300,
                5,
                232,
                236,
                2,
                19,
                63,
                85,
                "exampleMap3",
                "exampleVideo3",
                "exampleImage3",
                "exampleVideo3",
            )
        )
        # Add test organizations
        db.session.add(
            Organization(
                1,
                1,
                "FakeOrg",
                "Fake Cause",
                "FakeCity",
                "TX",
                77777,
                8000,
                30000,
                "FakeClass",
                96.2,
                123456,
                "NoCause",
                "url",
                "addr",
                "FakeMission",
                "map",
                "image",
            )
        )
        db.session.add(
            Organization(
                2,
                2,
                "FakeOrg2",
                "Fake Cause",
                "FakeCity2",
                "AL",
                73477,
                10000,
                33500,
                "FakeClass1",
                88.1,
                424523,
                "Tag",
                "url2",
                "addr2",
                "FakeMission2",
                "map2",
                "image2",
            )
        )
        db.session.add(
            Organization(
                3,
                3,
                "FakeOrg3",
                "Fake Cause3",
                "FakeCity3",
                "TX",
                23478,
                60000,
                100000,
                "FakeClass2",
                100,
                345214,
                "Tag2",
                "url3",
                "addr3",
                "FakeMission3",
                "map3",
                "image3",
            )
        )
        # Add test schools
        db.session.add(
            School(
                1,
                1,
                "FakeCity",
                "TX",
                "FakeSchool",
                "FakeDistrict",
                "Middle",
                98,
                320,
                34,
                3.2,
                20,
                18,
                11,
                20,
                1,
                50,
                3,
                "addr",
                0.0,
                0.0,
                "map",
                "video",
            )
        )
        db.session.add(
            School(
                2,
                2,
                "FakeCity2",
                "AL",
                "FakeSchool2",
                "FakeDistrict2",
                "High",
                88,
                3000,
                24,
                5.1,
                11,
                24,
                33,
                1,
                10,
                22,
                6,
                "addr2",
                1.1,
                1.1,
                "map2",
                "video2",
            )
        )
        db.session.add(
            School(
                3,
                3,
                "FakeCity3",
                "TX",
                "FakeSchool3",
                "FakeDistrict3",
                "Elementary",
                82,
                5000,
                11,
                10.1,
                12,
                21,
                14,
                6,
                13,
                44,
                2,
                "addr3",
                2.1,
                -1.1,
                "map3",
                "video3",
            )
        )

    # Delete test database
    def tearDown(self):
        db.session.remove()
        db.drop_all()


class FlaskTest(BaseTest):
    def test_base(self):
        response = self.client.get("/", content_type="html/text")
        self.assertEqual(response.status_code, 200)

    # testing contents of city object
    def test_city_single(self):
        response = self.client.get("/cities/1", content_type="application/json")
        # response ok
        self.assertEqual(response.status_code, 200)
        data = json.loads(response.get_data(as_text=True))
        correctData = {
            "id": 1,
            "city": "FakeCity",
            "state": "TX",
            "totalPopulation": 2_500_000,
            "medianIncome": 2000,
            "povertyPopulation": 5000,
            "unemployed": 500,
            "medianRent": 800,
            "vacantHousingPercent": 3,
            "foodStampsPercent": 8,
            "collegeDegreePercent": 76,
            "percentHealthInsured": 98,
            "map": "exampleMap",
            "video": "exampleVideo",
            "image": "exampleImage",
        }
        # returning all fields except school video
        self.assertEqual(len(data), len(correctData))
        # returning correct fields
        for key in correctData:
            self.assertEqual(correctData[key], data[key])

    # testing cities list
    def test_city_all(self):
        response = self.client.get("/cities", content_type="application/json")
        data = json.loads(response.get_data(as_text=True))
        self.assertEqual(len(data), 3)

    # test filtering by city id (should only return 2 cities)
    def test_city_filter(self):
        response = self.client.get("/cities?fpopulation=3", content_type="application/json")
        data = json.loads(response.get_data(as_text=True))
        self.assertEqual(len(data), 2)

    # test that invalid id returns no results
    def test_city_invalid_id(self):
        response = self.client.get("/cities/4", content_type="application/json")
        self.assertEqual(response.status_code, 200)
        data = json.loads(response.get_data(as_text=True))
        self.assertEqual(len(data), 0)

    # test that invalid filters are ignored
    def test_city_invalid_filter(self):
        response = self.client.get(
            "/cities?wrong=BreakTheServer", content_type="application/json"
        )
        self.assertEqual(response.status_code, 200)
        data = json.loads(response.get_data(as_text=True))
        self.assertEqual(len(data), 3)

    # testing contents of single org object
    def test_org_single(self):
        response = self.client.get("/organizations/2", content_type="application/json")
        # response ok
        self.assertEqual(response.status_code, 200)
        data = json.loads(response.get_data(as_text=True))
        correctData = {
            "id": 2,
            "cityId": 2,
            "name": "FakeOrg2",
            "cause": "Fake Cause",
            "city": "FakeCity2",
            "state": "AL",
            "zip": 73477,
            "assets": 10000,
            "income": 33500,
            "irs_class": "FakeClass1",
            "rating": 88.1,
            "ein": 424523,
            "tagline": "Tag",
            "url": "url2",
            "address": "addr2",
            "mission": "FakeMission2",
            "map": "map2",
            "image": "image2",
        }
        # returning all fields except school video
        self.assertEqual(len(data), len(correctData))
        # returning correct fields
        for key in correctData:
            self.assertEqual(correctData[key], data[key])

    # testing organizations list
    def test_org_all(self):
        response = self.client.get("/organizations", content_type="application/json")
        data = json.loads(response.get_data(as_text=True))
        self.assertEqual(len(data), 3)

    # test filtering by cityId (should only return 1 city)
    def test_org_filter(self):
        response = self.client.get(
            "/organizations?cityId=2", content_type="application/json"
        )
        data = json.loads(response.get_data(as_text=True))
        self.assertEqual(len(data), 1)

    # test that invalid id returns no results
    def test_org_invalid_id(self):
        response = self.client.get("/organizations/4", content_type="application/json")
        self.assertEqual(response.status_code, 200)
        data = json.loads(response.get_data(as_text=True))
        self.assertEqual(len(data), 0)

    # test that invalid filters are ignored
    def test_organizations_invalid_filter(self):
        response = self.client.get(
            "/organizations?wrong=BreakTheServer", content_type="application/json"
        )
        self.assertEqual(response.status_code, 200)
        data = json.loads(response.get_data(as_text=True))
        self.assertEqual(len(data), 3)

    # testing contents of single org object
    def test_school_single(self):
        response = self.client.get("/schools/3", content_type="application/json")
        # response ok
        self.assertEqual(response.status_code, 200)
        data = json.loads(response.get_data(as_text=True))
        correctData = {
            "id": 3,
            "cityId": 3,
            "city": "FakeCity3",
            "state": "TX",
            "schoolName": "FakeSchool3",
            "district": "FakeDistrict3",
            "schoolLevel": "Elementary",
            "rankPercentile": 82,
            "numStudents": 5000,
            "percentFreeLunch": 11,
            "studentTeacherRatio": 10.1,
            "pctAfricanAmerican": 12,
            "pctAsian": 21,
            "pctHispanic": 14,
            "pctIndian": 6,
            "pctPacificIslander": 13,
            "pctWhite": 44,
            "pctMixed": 2,
            "address": "addr3",
            "latitude": 2.1,
            "longitude": -1.1,
            "map": "map3",
            "video": "video3",
        }
        # returning all fields except school video
        self.assertEqual(len(data), len(correctData))
        # returning correct fields
        for key in correctData:
            self.assertEqual(correctData[key], data[key])

    # testing organizations list
    def test_school_all(self):
        response = self.client.get("/schools", content_type="application/json")
        data = json.loads(response.get_data(as_text=True))
        self.assertEqual(len(data), 3)

    # test filtering by state (should only return 2 cities)
    def test_school_filter(self):
        response = self.client.get("/schools?fstate=r", content_type="application/json")
        data = json.loads(response.get_data(as_text=True))
        self.assertEqual(len(data), 2)

    # test that invalid id returns no results
    def test_school_invalid_id(self):
        response = self.client.get("/schools/4", content_type="application/json")
        self.assertEqual(response.status_code, 200)
        data = json.loads(response.get_data(as_text=True))
        self.assertEqual(len(data), 0)

    # test that invalid filters are ignored
    def test_school_invalid_filter(self):
        response = self.client.get(
            "/schools?wrong=BreakTheServer", content_type="application/json"
        )
        self.assertEqual(response.status_code, 200)
        data = json.loads(response.get_data(as_text=True))
        self.assertEqual(len(data), 3)


if __name__ == "__main__":
    with open('report.xml', 'wb') as output:
        unittest.main(testRunner=
                      xmlrunner.XMLTestRunner(output=output),failfast = False, buffer = False, catchbreak = False)
