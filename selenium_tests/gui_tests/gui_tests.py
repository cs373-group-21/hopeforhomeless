# Modeled after tests from https://gitlab.com/jonathanhrandall/cs373-project2/-/tree/master/frontend/gui_tests

import unittest
import time
from selenium import webdriver
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.common.by import By
import sys
import os
from selenium.webdriver.common.keys import Keys

PATH = "./gui_tests/chromedriver.exe" # default, replaced with os path for chromedriver
BASE_URL = "https://hopeforhomeless.me/"
NO_SELENIUM_WINDOW = (os.environ.get("NO_SELENIUM_WINDOW", "true").lower() == "true")

# PATH = "./frontend/gui_tests/chromedriver.exe"
# BASE_URL = "https://www.homefarmer.me/"
# NO_SELENIUM_WINDOW = (os.environ.get("NO_SELENIUM_WINDOW", "true").lower() == "true")

class TestNavigation(unittest.TestCase):

    @classmethod
    def setUp(inst):
        options = webdriver.ChromeOptions()
        options.add_argument("--headless")
        options.add_argument("--disable-gpu")
        options.add_argument("--no-sandbox")
        options.add_argument("--start-maximized")
        options.add_argument("--window-size=1920,1080")
        options.add_argument('--disable-dev-shm-usage')  
        inst.driver = webdriver.Chrome(PATH, options=options)
        inst.driver.implicitly_wait(3)

        # Navigate to the application home page
        inst.driver.get(BASE_URL)

    @classmethod
    def tearDown(inst):
        # Close the browser window
        inst.driver.quit()

    def test_has_correct_title(self):
        title = self.driver.title
        self.assertTrue(title == 'Hope for Homeless')


    # Nav Bar Tests
    def test_can_navigate_to_cities(self):
        cities_link = self.driver.find_element_by_link_text('Cities')
        cities_link.click()
        time.sleep(1)
        self.assertTrue('locations' in self.driver.current_url)

    def test_can_navigate_to_schools(self):
        cities_link = self.driver.find_element_by_link_text('Schools')
        cities_link.click()
        time.sleep(1)
        self.assertTrue('schools' in self.driver.current_url)

    def test_can_navigate_to_orgs(self):
        cities_link = self.driver.find_element_by_link_text('Organizations')
        cities_link.click()
        time.sleep(1)
        self.assertTrue('organizations' in self.driver.current_url)

    def test_can_navigate_to_about(self):
        recipes_link = self.driver.find_element_by_link_text('About')
        recipes_link.click()
        time.sleep(1)
        self.assertTrue('about' in self.driver.current_url)

if __name__ == "__main__":

    try:
        # For linux headless only.
        # OK to fail & ignore when running in development.
        from pyvirtualdisplay import Display

        display = Display(visible=0, size=(1920, 1080))
        display.start()
    except ImportError as e:
        print('pyvirtualdisplay not found, no virtual display will be created.')
    PATH = sys.argv[1]
    unittest.main(argv=['first-arg-is-ignored'])





