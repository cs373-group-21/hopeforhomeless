Git SHA: 69f9a1b0273c7a885a40795654de98f116d50da1
Leader: Truman

Link: https://hopeforhomeless.me/
Git Pipeline: https://gitlab.com/cs373-group-21/hopeforhomeless/-/pipelines
Link to Final Presentation: https://youtu.be/IVNiE11qgAM

Name_1: Sruthi Rudravajjala
EID_1: sr46252
Gitlab_ID_1: sruthi_rudravajjala
Est. completion time: 15 hours
Actual completion time: 15 hours

Name_2: Shayan Maradia
EID_2: sm68862
Gitlab_ID_2: maradiai
Est. completion time: 15 hours
Actual completion time: 15 hours

Name_3: Truman Byrd
EID_3: tb29668
Gitlab_ID_3: trumanbyrd
Est. completion time: 15 hours
Actual completion time: 15 hours

Name_4: Noah Galloso
EID_4: nng369
Gitlab_ID_4: nngalloso
Est. completion time: 15 hours
Actual completion time: 15 hours

Name_5: Shikhar Gupta
EID_5: sg49255
Gitlab_ID_5: Shikhar-G
Est. completion time: 15 hours
Actual completion time: 15 hours

Notes: We used the "Burnin Up" project as a reference for searching, and we used some of their code
